(function env() {
  window.__env = window.__env || {};

  // Url commun à prodige

  /** Carto */
  window.__env.serverUrl = 'https://carto.prodige.internal';

  /** Catalogue */
  window.__env.catalogueUrl = 'https://catalogue.prodige.internal';

  /** Admincarto */
  window.__env.admincartoUrl = 'https://admincarto.prodige.internal';

  /** Datacarto */
  window.__env.datacarto = 'https://datacarto.prodige.internal';

  /** Cas */
  window.__env.casUrl = 'https://cas.prodige.internal';

  /** Admin */
  window.__env.urlAdmin = 'https://admin.prodige.internal';

  // Url Spécifique au projet

  /** Vues */
  window.__env.urlVues = 'https://vues.prodige.internal';

})();





