import { Component } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent } from '@angular/router';

@Component({
  selector:    `alk-root`,
  templateUrl: `./app.component.html`,
  styleUrls:   [`./app.component.scss`],
})
export class AppComponent {
  title = ``;
  protected loading: boolean = true;
  private appActive = false;

  constructor( private router: Router ) {
    router.events.subscribe(( routerEvent ) => {
      this.checkRouterEvent( <RouterEvent>routerEvent );
    });
  }

  checkRouterEvent( routerEvent: RouterEvent ): void {
    if ( routerEvent instanceof NavigationStart && !this.appActive ) {
      this.loading = true;
    }

    if ( routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationCancel || routerEvent instanceof NavigationError ) {
      this.loading = false;
    }
  }

  onAppActivate(){
    this.appActive = true;
  }
}
