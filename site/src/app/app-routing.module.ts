import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CasGuard } from './core/guards/cas.guard';
import { layerViewResolver } from './core/resolvers/layer-view.resolver';
import { layerRessourceResolver } from './core/resolvers/layer-ressource.resolver';
import { isAdminGuard } from './core/guards/is-admin.guard';

const routes: Routes = [
  {
    // TODO: à remplacer par une page d'erreur
    path:          `manage`,
    pathMatch:     `full`,
    canActivate:   [CasGuard],
    loadComponent: () => import( `./pages/manage/manage.component` ).then(( mod ) => mod.ManageComponent ),
  },
  {
    path:          `manage/:uuid`,
    pathMatch:     `full`,
    canActivate:   [CasGuard],
    loadComponent: () => import( `./pages/manage/manage.component` ).then(( mod ) => mod.ManageComponent ),
  }, {
    path:          `custom/:uuid`,
    pathMatch:     `full`,
    canActivate:   [ CasGuard, isAdminGuard ],
    loadComponent: () => import( `./pages/custom/custom.component` ).then(( mod ) => mod.CustomComponent ),
  },
  {
    path:          `create/:uuid/:layerId`,
    pathMatch:     `full`,
    canActivate:   [CasGuard],
    loadComponent: () => import( `./pages/create/create.component` ).then(( mod ) => mod.CreateComponent ),
    resolve:       { layerRessources: layerRessourceResolver, layerView: layerViewResolver  },
  },
  {
    path:       `**`,
    redirectTo: `manage`,
    pathMatch:  `full`,
  },
];
@NgModule({
  imports: [RouterModule.forRoot( routes )],
  exports: [RouterModule],
})
export class AppRoutingModule {

}
