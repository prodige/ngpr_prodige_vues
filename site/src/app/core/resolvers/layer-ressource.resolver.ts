import { ResolveFn } from '@angular/router';

import { inject } from '@angular/core';

import { Observable, switchMap } from 'rxjs';
import { LayerResource } from '../models/layer-resource';
import { AdminService } from '../services/prodige-api/admin.service';
import { LAYER_TYPE } from '../models/layer-type.model';

export const layerRessourceResolver: ResolveFn<LayerResource[]> = (): Observable<LayerResource[]> => {
  const adminService = inject( AdminService );
  return adminService.getLayers$( LAYER_TYPE.TABULAIRE ).pipe(
      switchMap(() => adminService.getLayers$( LAYER_TYPE.VECTOR )),
    );
};
