import { ResolveFn } from '@angular/router';
import { inject } from '@angular/core';
import { AdminService } from '../services/prodige-api/admin.service';
import { Observable } from 'rxjs';
import { LayerResource } from '../models/layer-resource';

export const layerViewResolver: ResolveFn<LayerResource> = ( route ): Observable<LayerResource> => {
  const layerId = parseInt( route.paramMap.get( `layerId` ), 10 );

  const adminService = inject( AdminService );
  return adminService.getLayer$( layerId );
};
