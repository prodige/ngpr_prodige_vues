import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculatedField } from '../../models/calculated-field';
import { VuesService } from '../../services/prodige-api/vues.service';
import { takeUntil } from 'rxjs';
import { CalculatedFieldModalComponent } from '../calculated-field-modal/calculated-field-modal.component';
import { TableReloadService } from '../../services/table-reload.service';
import { BaseComponent } from '../base/base.component';
import { SqlTypeLabelPipe } from '../../pipes/sql-type-label.pipe';


@Component({
  selector:    `alk-calculated-fields`,
  standalone:  true,
  imports:     [ CommonModule, CalculatedFieldModalComponent, SqlTypeLabelPipe ],
  templateUrl: `./calculated-fields.component.html`,
  styleUrls:   [`./calculated-fields.component.scss`],
})
export class CalculatedFieldsComponent extends BaseComponent implements OnInit{
  @Input({ required: true }) layerId: number;

  protected calculatedFields: CalculatedField[] = [];

  constructor(
    private vuesService: VuesService,
    private tableReloadService: TableReloadService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.tableReloadService.canReload$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(() =>  this.reloadTable());

    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( viewParams ) => {
        if ( viewParams?.calculated_fields && viewParams?.calculated_fields instanceof Array ){
          this.calculatedFields = viewParams?.calculated_fields;
        }
      });
  }

  delete( idx: number ){
    if ( this.calculatedFields[idx]){
      this.calculatedFields.splice( idx, 1 );
    }
  }
}
