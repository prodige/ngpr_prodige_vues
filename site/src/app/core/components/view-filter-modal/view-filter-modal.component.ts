import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '../base/base.component';
import { switchMap, takeUntil } from 'rxjs';
import { VuesService } from '../../services/prodige-api/vues.service';
import { ViewParams } from '../../models/view-params';
import { LayerFieldService } from '../../services/layer-field.service';
import { SQL_OPERATOR } from '../../models/constant-sql';
import { ProdigeField } from '../../models/prodige-field.model';

@Component({
  selector:    `alk-view-filter-modal`,
  standalone:  true,
  imports:     [ CommonModule, FormsModule, ReactiveFormsModule ],
  templateUrl: `./view-filter-modal.component.html`,
})
export class ViewFilterModalComponent extends BaseComponent implements OnInit{
  @Input({ required: true }) layerId: number;
  @Input() edit: boolean = false;

  protected viewParams: ViewParams =  null;

  protected fieldView :ProdigeField[] = [];

  protected sqlOperators = SQL_OPERATOR;

  protected oldExpression = ``;

  constructor(
    private modalService: NgbModal,
    private vuesService: VuesService,
    private layerFieldService: LayerFieldService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( viewParams ) => {
        this.viewParams = <ViewParams>JSON.parse( JSON.stringify( viewParams ));
        this.oldExpression = viewParams.filter;
        return  this.layerFieldService.loadAllFieldView( viewParams, this.layerId );
      }),
    ).subscribe(( fields ) => {
      this.fieldView = fields;
    });

  }


  open( content: TemplateRef<unknown> ) {
    const viewParams = this.vuesService.getViewParams();
    this.viewParams = <ViewParams>JSON.parse( JSON.stringify( viewParams ));
    this.oldExpression = viewParams.filter;

    // Timeout pour recharger l'affichage des tableaux
    setTimeout(() => {
      this.modalService.open( content, {
        backdrop: `static`,
        keyboard: false,
        size:     `xl`,
      });
    }, 250 );

  }

  validate( modal: NgbActiveModal ) {
    this.vuesService.setViewParams$( this.viewParams );
    modal.close();
  }

  cancel( modal: NgbActiveModal ){
    this.viewParams.filter = ``;
    modal.close();
  }

  addToExpression( text:  string, type: string = `operator` ){
    if ( type === `field` ){
      this.viewParams.filter === null ? `` : this.viewParams.filter;
      this.viewParams.filter += `[${text}]`;
    }
    else {
      this.viewParams.filter += text;
    }
  }
}
