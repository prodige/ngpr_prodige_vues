import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector:    `alk-form-error`,
  standalone:  true,
  imports:     [CommonModule],
  templateUrl: `./form-error.component.html`,
  styleUrls:   [`./form-error.component.scss`],
})
export class FormErrorComponent  implements OnInit{
  @Input() control: FormGroup | FormArray | AbstractControl;
  @Input() field!: string | number;

  public ctrlError: AbstractControl;

  ngOnInit() {
    if ( !this.control ) { throw new Error( `missing *form* input` ) }
    if ( this.field == undefined ) { throw new Error( `missing *controlName* input` ) }

    if ( this.control instanceof FormArray ) {
      this.ctrlError = this.control.at( <number> this.field );
    } else {
      this.ctrlError = this.control.get( <string> this.field );
    }

  }
}

