import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { map, of, switchMap, takeUntil } from 'rxjs';
import { VuesService } from '../../services/prodige-api/vues.service';
import { FieldTabParams } from '../../models/field-tab-params';
import { AGGREGATE_FUNCTION } from '../../models/constant-sql';
import { FormsModule } from '@angular/forms';
import { AdminService } from '../../services/prodige-api/admin.service';
import { LayerFieldService } from '../../services/layer-field.service';

import { ViewParams } from '../../models/view-params';
import { TableReloadService } from '../../services/table-reload.service';
import { BaseComponent } from '../base/base.component';

@Component({
  selector:    `alk-parameter-aggregate`,
  standalone:  true,
  imports:     [ CommonModule, NgbNavModule, FormsModule ],
  templateUrl: `./parameter-aggregate.component.html`,
})
export class ParameterAggregateComponent extends BaseComponent implements OnInit{
  protected aggregateFunction = AGGREGATE_FUNCTION;

  protected groupedField : {[key: string]: boolean} = {};

  protected navigation  :{active: FieldTabParams, fieldTabParams: FieldTabParams[]} = { active: null, fieldTabParams: []};

  protected viewParams: ViewParams;

  constructor(
    private vuesService: VuesService,
    private adminService: AdminService,
    private layerFieldService: LayerFieldService,
    private tableReloadService: TableReloadService,
  ) {
    super();
  }

  ngOnInit(): void {

    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( viewParams ) => {
        const canReload = !!this.viewParams;

        this.viewParams = <ViewParams>JSON.parse( JSON.stringify( viewParams ));
        return canReload ? this.tableReloadService.canReload$( `aggregate` ) : of( true );
      }),
      map(() => this.layerFieldService.buildAggregatTabs( this.viewParams, true )),
    )
      .subscribe(( navigation ) => {
        if ( navigation ){
          this.navigation = navigation;
          this.buildGroupingField();
          this.vuesService.setViewParams$( this.viewParams );
        }
      });
  }

  onGroupedCheck( field: string, selected: boolean ){
    this.viewParams.grouping_fields = [];
    this.groupedField[field] = selected;
    Object.entries( this.groupedField ).forEach(([ field, selected ])=> {
      if ( selected ){
        this.viewParams.grouping_fields.push( field );
      }
    });

    this.vuesService.setViewParams$( this.viewParams );
  }

  buildGroupingField(){
    this.groupedField = {};
    this.viewParams.grouping_fields.forEach(( field ) => {
      this.groupedField[field] = true;
    });
  }

  onMethodChange( field: string, method: string ){
    console.log( method, method !== `null`, this.viewParams.grouping_method, field );
    if ( method && method !== `null` ){
      this.viewParams.grouping_method[field] = method;
      this.onGroupedCheck( field, false );
    }
    else {
      delete this.viewParams.grouping_method[field];
      this.onGroupedCheck( field, true );
    }

    this.vuesService.setViewParams$( this.viewParams );
  }

  onSelectGeom( tab: FieldTabParams ){
    if ( !tab.geomFunction || tab.geomFunction === `null` ){
      this.navigation.fieldTabParams[0].geomFunction = null;
      this.viewParams.grouping_method = {};
      this.viewParams.grouping_fields = [];
      this.groupedField = {};
    }
    else {
      if ( !this.viewParams.grouping_method[`the_geom`]){
        this.viewParams.grouping_method = {};
        this.viewParams.grouping_fields = [];
        this.viewParams.visible_fields.forEach(( field ) =>{
          this.viewParams.grouping_method[`${this.viewParams.prefix}${field}`] = null;
          this.viewParams.grouping_fields.push( `${this.viewParams.prefix}${field}` );
          this.groupedField[`${this.viewParams.prefix}${field}`] = true;
        });

        this.viewParams.joins.forEach(( join ) =>{
          join.visible_fields.forEach(( field ) =>{
            this.viewParams.grouping_method[`${join.prefix}${field}`] = null;
            this.viewParams.grouping_fields.push( `${join.prefix}${field}` );
            this.groupedField[`${join.prefix}${field}`] = true;
          });
        });

      }
      this.viewParams.grouping_method[`the_geom`] = tab.geomFunction;
    }

    this.vuesService.setViewParams$( this.viewParams );
  }
}
