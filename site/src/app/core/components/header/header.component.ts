import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IUserRight } from '../../models/user-right.model';
import { UserService } from '../../services/user.service';
import { Observable } from 'rxjs';
import { ModalConfirmComponent } from '../modal-confirm/modal-confirm.component';
import { EnvService } from '../../services/env/env.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CasService } from '../../services/cas/cas.service';

@Component({
  selector:    `alk-header`,
  standalone:  true,
  imports:     [CommonModule],
  templateUrl: `./header.component.html`,
})
export class HeaderComponent implements OnInit{
  @Input() viewName: string;
  @Input() uuid: string;
  @Input() dataset: string;
  private message = $localize`:@@headerConfirm:Êtes-vous sûr de vouloir quitter ce menu ?  Toutes les informations non enregistrées seront perdues.`;

  protected user$: Observable<IUserRight>;

  constructor(
    private userService: UserService,
    private envService: EnvService,
    private ngbModal: NgbModal,
    private casService: CasService,
  ) {
    this.user$ = userService.getUser$();
  }

  ngOnInit(): void {
    this.userService.loadUser();
  }
  loggout(){
    const modalRef = this.ngbModal.open( ModalConfirmComponent );
    ( <ModalConfirmComponent>modalRef.componentInstance ).message = this.message;

    modalRef.result.then(( confirmation ) => {
      if ( confirmation ) {
        this.casService.logout();
      }
    })
      .catch(( err ) => {
        console.error( err );
      });
  }

}
