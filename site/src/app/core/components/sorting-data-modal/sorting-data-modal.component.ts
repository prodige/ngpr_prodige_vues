import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewParams, ViewSort } from '../../models/view-params';
import { VuesService } from '../../services/prodige-api/vues.service';
import { AdminService } from '../../services/prodige-api/admin.service';
import { LayerFieldService } from '../../services/layer-field.service';
import { TableReloadService } from '../../services/table-reload.service';
import { switchMap, takeUntil } from 'rxjs';
import { BaseComponent } from '../base/base.component';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SQL_SORT_LABELS } from '../../models/constant-sql';
import { FormErrorComponent } from '../form-error/form-error.component';
import { ProdigeField } from '../../models/prodige-field.model';

@Component({
  selector:    `alk-sorting-data-modal`,
  standalone:  true,
  imports:     [ CommonModule, ReactiveFormsModule, FormErrorComponent ],
  templateUrl: `./sorting-data-modal.component.html`,
})
export class SortingDataModalComponent extends BaseComponent implements OnInit{
  @Input({ required: true }) layerId: number;
  @Input() edit: boolean = false;

  protected viewParams: ViewParams;

  protected haveSort: boolean = false;

  protected oldSort: ViewSort[] = [];

  protected fieldView: ProdigeField[] = [];

  protected sqlSortLabels = SQL_SORT_LABELS;

  protected sortFormGroup :FormGroup;

  constructor(
    private vuesService: VuesService,
    private adminService: AdminService,
    private layerFieldService: LayerFieldService,
    private tableReloadService: TableReloadService,
    private modalService: NgbModal,
  ) {
    super();
  }

  ngOnInit(): void {

    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( viewParams ) => {
        this.initView();

        return  this.layerFieldService.loadAllFieldView( viewParams, this.layerId );
      }),
    )
      .subscribe(( fields ) => {
        this.fieldView = fields;
      });
  }

  validate( modal: NgbActiveModal ) {
    this.sortFormGroup.markAllAsTouched();


    if ( !this.sortFormGroup.valid ) {
      return;
    }

    const sqlSortValue = <ViewSort> this.sortFormGroup.getRawValue();
    this.viewParams.sort = [sqlSortValue];
    this.vuesService.setViewParams$( this.viewParams );
    this.tableReloadService.emitReload$();
    modal.close();
  }

  cancel( modal: NgbActiveModal ){
    this.viewParams.sort = [];
    modal.close();
  }

  open( content: TemplateRef<unknown> ) {
    this.initView();

    // Timeout pour recharger l'affichage des tableaux
    setTimeout(() => {
      this.modalService.open( content, {
        backdrop: `static`,
        keyboard: false,
        size:     `xl`,
      });
    }, 250 );

  }


  buidlFormGroup() {
    return new FormGroup({
      field:   new FormControl( this.viewParams.sort[0].field, [Validators.required]),
      method:  new FormControl( this.viewParams.sort[0].method, [Validators.required]),
    });
  }

  private initView(){
    const viewParams = this.vuesService.getViewParams();
    this.viewParams = <ViewParams>JSON.parse( JSON.stringify( viewParams ));
    this.oldSort = viewParams.sort;
    this.haveSort = !!viewParams.sort?.length;
    if ( !this.haveSort ){
      this.viewParams.sort = [{ field: null, method: null }];
    }
    this.sortFormGroup = this.buidlFormGroup();
  }
}
