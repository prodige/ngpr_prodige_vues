import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculatedFieldModalComponent } from '../calculated-field-modal/calculated-field-modal.component';
import { takeUntil } from 'rxjs';
import { VuesService } from '../../services/prodige-api/vues.service';
import { TableReloadService } from '../../services/table-reload.service';
import { BaseComponent } from '../base/base.component';
import { ViewParams } from '../../models/view-params';
import { ViewFilterModalComponent } from '../view-filter-modal/view-filter-modal.component';

@Component({
  selector:    `alk-view-filter`,
  standalone:  true,
  imports:     [ CommonModule, CalculatedFieldModalComponent, ViewFilterModalComponent ],
  templateUrl: `./view-filter.component.html`,
  styleUrls:   [`./view-filter.component.scss`],
})
export class ViewFilterComponent extends BaseComponent implements OnInit{
  @Input({ required: true }) layerId: number;
  protected viewParams: ViewParams;

  constructor(
    private vuesService: VuesService,
    private tableReloadService: TableReloadService,
  ) {
    super();
  }
  ngOnInit(): void {
    this.tableReloadService.canReload$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(() =>  this.reloadTable());

    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( viewParams ) => {
        this.viewParams = viewParams;
      });
  }

  delete(){
    this.viewParams.filter = ``;
    this.vuesService.setViewParams$( this.viewParams );
  }
}
