import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JoinWarning } from '../../models/join-warning.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector:    `alk-join-warning-modal`,
  standalone:  true,
  imports:     [ CommonModule, FormsModule, ReactiveFormsModule ],
  templateUrl: `./join-warning-modal.component.html`,
  styleUrls:   [`./join-warning-modal.component.scss`],
})
export class JoinWarningModalComponent {
  @Input() joinWarning: JoinWarning;

  constructor( private activeModal: NgbActiveModal ) {}

  close(){
    this.activeModal.close();
  }
}
