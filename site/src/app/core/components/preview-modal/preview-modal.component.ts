import { Component, Input, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { FormErrorComponent } from '../form-error/form-error.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PreviewModalService } from './preview-modal.service';

@Component({
  selector:    `alk-preview-modal`,
  standalone:  true,
  imports:     [ CommonModule, FormErrorComponent, ReactiveFormsModule ],
  templateUrl: `./preview-modal.component.html`,
  styleUrls:   [`./preview-modal.component.scss`],
})
export class PreviewModalComponent{
  @Input() uuid: string;

  protected propperties$: Observable<string[][]>;

  protected keys$ :Observable<string[]>;

  constructor(
    private previewService: PreviewModalService,
    private modalService: NgbModal,
  ) {
    this.propperties$ = this.previewService.getProperties$();
    this.keys$ = this.previewService.getKeys$();
  }

  close( modal: NgbActiveModal ){
    modal.close();
    this.previewService.clear();
  }

  open( content: TemplateRef<unknown> ) {
    this.previewService.loadGeojson( this.uuid );

    // Timeout pour recharger l'affichage des tableaux
    setTimeout(() => {
      this.modalService.open( content, {
        backdrop: `static`,
        keyboard: false,
        size:     `xl`,
      });
    }, 250 );

  }

}
