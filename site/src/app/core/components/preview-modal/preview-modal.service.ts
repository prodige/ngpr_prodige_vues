import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject } from 'rxjs';

import { EnvService } from '../../services/env/env.service';
import { LoggerService } from '../logger/logger.service';
import { AlkGeojson } from '../../models/geojson.model';

@Injectable({
  providedIn: `root`,
})
export class PreviewModalService {

  private keys$ = new BehaviorSubject<string[]>([]);
  private properties$ = new BehaviorSubject<string[][]>([]);

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
    private loggerService: LoggerService,
  )
  { }

  getKeys$() { return this.keys$.asObservable() }

  getProperties$() { return this.properties$.asObservable() }

  loadGeojson( uuid: string ): void{
    this.httpClient.get<AlkGeojson>( `${this.envService.urlVues}/api/view/data/${uuid}` )
      .subscribe({
        next: ( geojson ) =>{
          if ( !geojson.features.length || !geojson.features[0].properties ){
            return;
          }
          const properties = geojson.features.map(( feature ) => Object.values( feature.properties ));

          this.keys$.next( Object.keys( geojson.features[0].properties ));
          this.properties$.next( properties  );
        },
        error: ( err ) =>{
          console.error( err );
          this.clear();
          this.loggerService.errorOnModal$( `Erreur survenue pendant le chargement des données` );
        },
      });
  }

  clear(){
    this.keys$.next([]);
    this.properties$.next([]);
  }
}
