import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewParams } from '../../models/view-params';
import { VuesService } from '../../services/prodige-api/vues.service';
import { TableReloadService } from '../../services/table-reload.service';
import { takeUntil } from 'rxjs';
import { BaseComponent } from '../base/base.component';
import { ViewFilterModalComponent } from '../view-filter-modal/view-filter-modal.component';
import { SortingDataModalComponent } from '../sorting-data-modal/sorting-data-modal.component';
import { SqlSortLabelPipe } from '../../pipes/sql-sort-label.pipe';

@Component({
  selector:    `alk-sorting-data-list`,
  standalone:  true,
  imports:     [
    CommonModule,
    ViewFilterModalComponent,
    SortingDataModalComponent,
    SqlSortLabelPipe,
  ],
  templateUrl: `./sorting-data-list.component.html`,
})
export class SortingDataListComponent extends BaseComponent implements OnInit{
  @Input({ required: true }) layerId: number;
  protected viewParams: ViewParams;

  constructor(
    private vuesService: VuesService,
    private tableReloadService: TableReloadService,
  ) {
    super();
  }
  ngOnInit(): void {
    this.tableReloadService.canReload$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(() =>  this.reloadTable());

    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( viewParams ) => {
        this.viewParams = viewParams;
        console.log( this.viewParams.sort );
      });
  }

  delete(){
    this.viewParams.sort = [];
    this.vuesService.setViewParams$( this.viewParams );
    this.tableReloadService.emitReload$();
  }
}
