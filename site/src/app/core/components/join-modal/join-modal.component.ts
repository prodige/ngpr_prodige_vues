import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../../services/prodige-api/admin.service';
import { LayerResource } from '../../models/layer-resource';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { JoinParams } from '../../models/join-params';
import { VuesService } from '../../services/prodige-api/vues.service';
import { delay, of, Subscription, switchMap, takeUntil, tap } from 'rxjs';
import { ViewParams } from '../../models/view-params';
import { LayerFieldService } from '../../services/layer-field.service';
import { TableReloadService } from '../../services/table-reload.service';
import { BaseComponent } from '../base/base.component';
import { JoinWarning } from '../../models/join-warning.model';
import { FormErrorComponent } from '../form-error/form-error.component';
import { ProdigeField } from '../../models/prodige-field.model';
import { LAYER_TYPE } from '../../models/layer-type.model';
import { OrderStringByPipePipe } from '../../pipes/order-string-by.pipe';

@Component({
  selector:   `alk-join-modal`,
  standalone: true,
  imports:    [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormErrorComponent,
    OrderStringByPipePipe,
  ],
  templateUrl: `./join-modal.component.html`,
})
export class JoinModalComponent extends BaseComponent implements OnInit {
  @Input({ required: true }) layerId: number;
  @Input() joinIdx: number = null;

  protected layerRessources: LayerResource[] = [];

  protected layerRessource: LayerResource = null;

  protected layerJoinId: number = null;

  protected fieldsTable: string[] = [];

  protected fieldsView: ProdigeField[] = [];

  protected joinFormGroup: FormGroup = null;

  protected joinParams: JoinParams;

  protected joinWarning: JoinWarning;

  private joinList: JoinParams[] = [];

  private subLayer: Subscription = null;

  private viewParams: ViewParams = null;

  protected tableFields:{[key: string]: string} = null;
  constructor(
    private modalService: NgbModal,
    private adminService: AdminService,
    private vuesService: VuesService,
    private layerFieldService: LayerFieldService,
    private tableReloadService: TableReloadService,
  ) {
    super();
  }

  ngOnInit() {
    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( viewsParams ) => {
        this.viewParams = viewsParams;
        this.joinList = viewsParams?.joins;
        return this.adminService.getLayers$( LAYER_TYPE.TABULAIRE );
      }),
      switchMap(( layerRessources ) => {
        this.layerRessources = layerRessources?.filter(
          ( layer ) => layer.id !== this.layerId,
        );

        let layerTmp = null;
        if ( this.layerRessources instanceof Array && ( this.joinIdx || this.joinIdx === 0 ) && this.joinList[this.joinIdx]) {
          this.joinParams = this.joinList[this.joinIdx];
          layerTmp = this.layerRessources.find(( layer ) => layer.storagePath === this.joinParams.dataset );
        }

        // création d'une jointure
        if ( !this.joinParams || !this.viewParams.joins?.length ) {
          this.joinParams = this.buidJoinParams();
        }

        // recherche des données de la jointure pour récupérer les champs
        if ( layerTmp ) {
          this.layerJoinId = layerTmp.id;
          this.layerRequest( this.layerJoinId );
        }

        // création du formulaire
        this.joinFormGroup = this.buidlFormGroup();

        return this.layerFieldService.loadAllFieldView( this.viewParams, this.layerId );
      }))
      .subscribe(( fields ) => {
        this.fieldsView = fields;
      });
  }

  open( content: TemplateRef<unknown> ) {
    this.joinWarning = null;
    this.layerJoinId = this.joinIdx !== null ? this.layerJoinId : null;
    this.fieldsTable = this.joinIdx !== null ? this.fieldsTable : null;

    // Timeout pour recharger l'affichage des tableaux
    setTimeout(() => {
      if ( !this.joinParams ) {
        this.joinParams = this.buidJoinParams();
      }

      this.modalService.open( content, {
        backdrop: `static`,
        keyboard: false,
        size:     `xl`,
      });
    }, 250 );

  }

  layerRequest( layerId: number ) {
    this.subLayer = of( layerId ).pipe(
      tap(() => this.isLoading = true ),
      takeUntil( this.endSubscriptions ),
      delay( 1000 ),
      switchMap(() => this.layerFieldService.getLayerField$( layerId )),
      switchMap(( result ) => {
        this.tableFields = result.dbFields;
        const fields = result && result.dbFields ? Object.keys( result.dbFields ) : [];

        if ( this.joinIdx !== null ) {
          this.fieldsTable = fields.map(( field ) => `${this.joinParams.prefix}${field}` );
          const checkField = this.fieldsTable.find(( field ) => this.joinParams.field_table === field );
          this.fieldsTable = checkField ? this.fieldsTable : fields;
        } else {
          this.fieldsTable = fields;
        }

        return this.adminService.getLayer$( layerId );
      }),
    )
      .subscribe({
        next: ( layer ) => {

          this.layerRessource = layer;

          this.joinFormGroup.patchValue({
            resource:       this.layerRessource.metadataUuid,
            dataset:        this.layerRessource.storagePath,
            visible_fields: this.fieldsTable,
          });

          this.onSelectField();
        },
        error:    ( error ) => console.error( error ),
        complete: () => this.isLoading = false,
      });
  }

  onSelectTable( $event: Event ) {
    // pour limiter le nombre de requête au serveur
    if ( this.subLayer ) {
      this.subLayer.unsubscribe();
    }

    const layerId = parseInt(( <HTMLInputElement>$event.target ).value, 10 );
    this.layerRequest( layerId );
  }

  buidJoinParams(): JoinParams {
    console.log( this.viewParams.joins.length );
    return {
      resource:       null,
      dataset:        null,
      field_view:     null,
      field_table:    null,
      restrictive:    false,
      visible_fields: [],
      join_pos:       this.viewParams.joins.length,
      prefix:         `t${this.viewParams.joins.length}_`,
    };
  }

  /**
   *
   * Chargement des champs de la vues
   */
  buidlFormGroup() {

    return new FormGroup({
      resource:       new FormControl( this.joinParams.resource, [Validators.required]),
      dataset:        new FormControl( this.joinParams.dataset, [Validators.required]),
      field_view:     new FormControl( this.joinParams.field_view, [Validators.required]),
      field_table:    new FormControl( this.joinParams.field_table, [Validators.required]),
      restrictive:    new FormControl( this.joinParams.restrictive, [Validators.required]),
      visible_fields: new FormControl( this.joinParams.visible_fields, [Validators.required]),
      join_pos:       new FormControl( this.joinParams.join_pos ),
      prefix:         new FormControl( this.joinParams.prefix ),
    });
  }

  validate( modal: NgbActiveModal ) {
    this.joinFormGroup.markAllAsTouched();

    if ( !this.joinFormGroup.valid ) {
      return;
    }

    const oldJoins = this.viewParams.joins.slice();
    if (( this.joinIdx || this.joinIdx === 0 ) && this.joinList[this.joinIdx]) {
      // édition
      this.joinList[this.joinIdx] = <JoinParams> this.joinFormGroup.getRawValue();
    } else {
      // création
      this.joinList.push( <JoinParams> this.joinFormGroup.getRawValue());
    }

    this.vuesService.onChangeJoins().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( error ) => {
        if ( error.inCalculated || error.inSort || error.inFilter ) {
          this.joinWarning = error;
          this.viewParams.joins = oldJoins;
          this.vuesService.setViewParams$( this.viewParams );
          return;
        }

        this.viewParams.joins = this.joinList;
        this.vuesService.setViewParams$( this.viewParams );
        this.tableReloadService.emitReload$();

        // RAZ
        this.joinParams = this.buidJoinParams();
        this.joinFormGroup = this.buidlFormGroup();

        modal.close();
      });
  }

  onSelectField(){
    this.fieldsTable = [];
    const values = <JoinParams> this.joinFormGroup.getRawValue();
    if ( !values?.field_view ){
      return;
    }
    const fieldView = this.fieldsView.find(( field ) => field.name === values.field_view );
    if ( !fieldView.type ){
      return;
    }

    this.fieldsTable = Object.entries( this.tableFields )
      .filter(([ , value ]) =>
        this.getType( value ) === this.getType( fieldView.type ),
      )
      .map(([key]) => key );
  }

  getType( type: string ){
    let inputType: string = null;
    switch ( type ) {
      case `double precision`:
      case `double`:
      case `bigint`:
      case `real`:
      case `integer`:
        inputType = `number`;
        break;
      case `date`:
        inputType = `date`;
        break;
      case `character varying`:
        inputType = `text`;
        break;

      case `boolean`:
        inputType = `radio`;
        break;

      case `text`:
        inputType = `text`;
        break;
      case `file`:
        inputType = `file`;
        break;
      case `ARRAY`:
        inputType = `checkbox`;
        break;
      default:
        break;
    }

    return inputType;

  }
}
