import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { takeUntil } from 'rxjs';
import { LoggerService } from '../logger.service';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector:    `alk-modal-logger`,
  templateUrl: `./modal-logger.component.html`,
  styleUrls:   [`./modal-logger.component.scss`],
})
export class ModalLoggerComponent extends  BaseComponent implements OnInit {
  @ViewChild( `content` ) modalContent: ElementRef;

  public message = ``;

  private modalRef:  NgbModalRef | null = null;

  constructor(
    private ngbModal: NgbModal,
    private displayErrorService: LoggerService,
  )
  {
    super();
  }

  ngOnInit(): void {
    this.displayErrorService.getErrorOnModal$().pipe(
      takeUntil( this.endSubscriptions ),
    ).subscribe(( message ) => {
      this.displayMessage( message );
    });
  }

  public displayMessage( message: string ): void{
    /** */
    if ( !this.modalRef ){
      this.message = message;
      this.modalRef = this.ngbModal.open( this.modalContent );
    }

    this.modalRef.result.finally(() => this.modalRef = null ).catch( console.error );
  }
}
