import { Component, OnInit } from '@angular/core';
import { LoggerService } from '../logger.service';
import { takeUntil } from 'rxjs';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector:    `alk-toast-logger`,
  templateUrl: `./toast-logger.component.html`,
  styleUrls:   [`./toast-logger.component.scss`],
})
export class ToastLoggerComponent extends BaseComponent implements OnInit {
  public toasts: string[] = [];

  constructor(
    private loggerService: LoggerService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.loggerService.getErrorOnTaoast().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( toasts ) => this.toasts = toasts );
  }

  remove( toast: string ): void {
    this.loggerService.remove$( toast );
  }
}
