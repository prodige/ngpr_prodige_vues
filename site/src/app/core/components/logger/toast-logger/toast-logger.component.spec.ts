import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToastLoggerComponent } from './toast-logger.component';

describe('ToastLoggerComponent', () => {
  let component: ToastLoggerComponent;
  let fixture: ComponentFixture<ToastLoggerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToastLoggerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToastLoggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
