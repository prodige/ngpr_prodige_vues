import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JoinParams } from '../../models/join-params';
import { JoinModalComponent } from '../join-modal/join-modal.component';
import { takeUntil } from 'rxjs';
import { VuesService } from '../../services/prodige-api/vues.service';
import { BooleanTextPipe } from '../../pipes/boolean-text.pipe';
import { ViewParams } from '../../models/view-params';

import { TableReloadService } from '../../services/table-reload.service';
import { BaseComponent } from '../base/base.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JoinWarningModalComponent } from '../join-warning-modal/join-warning-modal.component';

@Component({
  selector:    `alk-join-list`,
  standalone:  true,
  imports:     [ CommonModule, JoinModalComponent, BooleanTextPipe ],
  templateUrl: `./join-list.component.html`,
  styleUrls:   [`./join-list.component.scss`],
})
export class JoinListComponent extends BaseComponent implements OnInit{
  @Input({ required: true }) layerId: number;

  protected viewParams: ViewParams;

  protected joinParams: JoinParams[] = [];

  constructor(
    private vuesService: VuesService,
    private tableReloadService: TableReloadService,
    private modalService: NgbModal,
  ) {
    super();
  }

  ngOnInit(): void {
    this.tableReloadService.canReload$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(() =>  this.reloadTable());

    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe(( viewParams ) => {
        this.viewParams = <ViewParams>JSON.parse( JSON.stringify( viewParams ));
        this.joinParams = this.viewParams?.joins;
      });

  }

  delete( idx: number ){
    if ( this.joinParams[idx]){
      const joinCopy = this.joinParams.slice();
      this.joinParams.splice( idx, 1 );
      this.vuesService.setViewParams$( this.viewParams );

      this.vuesService.onChangeJoins().pipe(
        takeUntil( this.endSubscriptions ),
      )
        .subscribe(( error ) =>{
          console.log( `delete`, joinCopy, error );
          if ( error.inCalculated || error.inSort || error.inFilter ){
            this.viewParams.joins = joinCopy;
            this.vuesService.setViewParams$( this.viewParams );
            const modalRef = this.modalService.open( JoinWarningModalComponent );
            ( <JoinWarningModalComponent>modalRef.componentInstance ).joinWarning = error;
          }
          this.tableReloadService.emitReload$();
      });
    }
  }
}
