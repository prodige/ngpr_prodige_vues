import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinListComponent } from './join-list.component';
import {CommonModule} from '@angular/common';
import {JoinModalComponent} from '../join-modal/join-modal.component';
import {BooleanTextPipe} from '../../pipes/boolean-text.pipe';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('JoinListComponent', () => {
  let component: JoinListComponent;
  let fixture: ComponentFixture<JoinListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, JoinModalComponent, BooleanTextPipe, HttpClientTestingModule ]
    });
    fixture = TestBed.createComponent(JoinListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
