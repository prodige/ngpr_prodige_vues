import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatedFieldModalComponent } from './calculated-field-modal.component';

describe('CalculatedFieldModalComponent', () => {
  let component: CalculatedFieldModalComponent;
  let fixture: ComponentFixture<CalculatedFieldModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CalculatedFieldModalComponent]
    });
    fixture = TestBed.createComponent(CalculatedFieldModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
