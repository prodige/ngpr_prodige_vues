import { Component, Input, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewParams } from '../../models/view-params';
import { VuesService } from '../../services/prodige-api/vues.service';
import { switchMap, takeUntil } from 'rxjs';
import { CalculatedField } from '../../models/calculated-field';
import { SQL_OPERATOR, SQL_TYPE } from '../../models/constant-sql';
import { LayerFieldService } from '../../services/layer-field.service';
import { BaseComponent } from '../base/base.component';
import { ProdigeField } from '../../models/prodige-field.model';

type FieldModalState = 'SELECT_TYPE' | 'WRITE_CALCUL';

@Component({
  selector:    `alk-calculated-field-modal`,
  standalone:  true,
  imports:     [ CommonModule, ReactiveFormsModule, FormsModule ],
  templateUrl: `./calculated-field-modal.component.html`,
  styleUrls:   [`./calculated-field-modal.component.scss`],
})
export class CalculatedFieldModalComponent extends BaseComponent implements OnInit, OnDestroy {
  @Input({ required: true }) layerId: number;
  @Input() fieldIdx: number = null;

  protected prodigeType = SQL_TYPE;

  protected fieldView :ProdigeField[] = [];

  protected fieldModalState: FieldModalState = `SELECT_TYPE`;

  protected sqlOperators = SQL_OPERATOR;

  protected calculatedField: CalculatedField = null;

  protected viewParams: ViewParams =  null;

  constructor(
    private modalService: NgbModal,
    private vuesService: VuesService,
    private layerFieldService: LayerFieldService,
  ) {
    super();
  }

  open( content: TemplateRef<unknown> ) {
    this.modalService.open( content, {
      backdrop: `static`,
      keyboard: false,
      size:     `xl`,
    });

    this.fieldModalState = `SELECT_TYPE`;
  }

  validate( modal: NgbActiveModal ){

    if (( !this.fieldIdx && this.fieldIdx !== 0 ) && this.calculatedField.name && this.calculatedField.type && this.calculatedField.expression ){
      this.viewParams.calculated_fields.push( this.calculatedField );
    }
    this.vuesService.setViewParams$( this.viewParams );
    modal.close();
  }

  ngOnInit(): void {
    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( viewParams ) => {
        this.viewParams = viewParams;
        this.loadCalculatedField();
        return  this.layerFieldService.loadAllFieldView( viewParams, this.layerId );
      }),
    ).subscribe(( fields ) => {
      this.fieldView = fields;
    });

  }
  override ngOnDestroy(): void {
   super.ngOnDestroy();
  }

  addToExpression( text:  string, type: string = `operator` ){
    if ( type === `field` ){
      this.calculatedField.expression === null ? `` : this.calculatedField.expression;
      this.calculatedField.expression += `[${text}]`;
    }
    else {
      this.calculatedField.expression += text;
    }
  }

  loadCalculatedField(){
    if (( this.fieldIdx ||  this.fieldIdx === 0 )  && this.viewParams?.calculated_fields && this.viewParams?.calculated_fields[this.fieldIdx]){
      this.calculatedField = this.viewParams?.calculated_fields[this.fieldIdx];
    }
    else {
      this.calculatedField = {
        name:       null,
        expression: ``,
        type:       null,
      };
    }
  }
}
