import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { of, switchMap, takeUntil } from 'rxjs';
import { VuesService } from '../../services/prodige-api/vues.service';
import { ViewParams } from '../../models/view-params';
import {  NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { FieldTabParams } from '../../models/field-tab-params';

import { AdminService } from '../../services/prodige-api/admin.service';
import { LayerFieldService } from '../../services/layer-field.service';
import { TableReloadService } from '../../services/table-reload.service';
import { BaseComponent } from '../base/base.component';
import { FormsModule } from '@angular/forms';
import { JoinParams } from '../../models/join-params';
import { SqlTypeLabelPipe } from '../../pipes/sql-type-label.pipe';


@Component({
  selector:    `alk-field-visibility`,
  standalone:  true,
  imports:     [ CommonModule, NgbNavModule, FormsModule, SqlTypeLabelPipe ],
  templateUrl: `./field-visibility.component.html`,
})
export class FieldVisibilityComponent extends BaseComponent implements OnInit{
  protected navigation  :{active: FieldTabParams, fieldTabParams: FieldTabParams[]} = { active: null, fieldTabParams: []};

  protected viewParams: ViewParams;

  protected sortField: string = null;

  constructor(
    private vuesService: VuesService,
    private adminService: AdminService,
    private layerFieldService: LayerFieldService,
    private tableReloadService: TableReloadService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.vuesService.getViewParams$().pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( viewParams ) => {
        console.log( `NEW VIEWPARAMS ` );
        const canReload = !!this.viewParams;

        this.viewParams = <ViewParams>JSON.parse( JSON.stringify( viewParams ));

        return canReload ? this.tableReloadService.canReload$() : of( true );
      }),
      switchMap(() => this.layerFieldService.buildTabs$( this.viewParams )),
    )
      .subscribe(( navigation ) => {
        console.log( `NEW VIEWPARAMS navigation `, navigation );
        if ( navigation ){
          this.navigation = navigation;
        }

        this.buildSelected();
      });
  }

  onSelect( field: string, fieldTabParams: FieldTabParams ){
    this.applyFieldSelected( this.viewParams, field, fieldTabParams );

    if ( this.viewParams?.joins?.length ){
      this.viewParams.joins.forEach(( joins ) =>{
        this.applyFieldSelected( joins, field, fieldTabParams );
      });
    }

    this.vuesService.setViewParams$( this.viewParams );
    this.tableReloadService.emitReload$( `aggregate` );
  }

  private applyFieldSelected( params: ViewParams|JoinParams, field: string, fieldTabParams: FieldTabParams ){

    if ( params.prefix === fieldTabParams.prefix ){
      const idxField = params.visible_fields.indexOf( field );
      // ajout
      if ( fieldTabParams.selected[field] && idxField === -1 ){
        params.visible_fields.push( field );
      }
      // suppression
      if ( !fieldTabParams.selected[field] && idxField >= 0 ){
        params.visible_fields.splice( idxField, 1 );
      }
    }
  }

  /*
   * transformation des tableau des champs visible, en objet de sélection {[key: string]:boolean};
   */
  private buildSelected(){
    console.log( this.navigation, this.viewParams );
    this.sortField = null;
    if ( this.viewParams?.sort?.length ){
      this.sortField = this.viewParams.sort[0].field;
    }
    if ( this.navigation?.fieldTabParams?.length ){
      this.navigation?.fieldTabParams.forEach(( fieldTabParams )=>{
        this.applyBuildSelected( this.viewParams, fieldTabParams );
        if ( this.viewParams?.joins?.length ){
          this.viewParams.joins.forEach(( joins ) =>{
            this.applyBuildSelected( joins, fieldTabParams );
          });
        }

      });
    }
  }

  private applyBuildSelected( params: ViewParams|JoinParams, fieldTabParams: FieldTabParams ){
    if (( params.prefix === fieldTabParams.prefix ) && params.visible_fields?.length ){
      const visibleSortField = params.visible_fields.find(( field ) => `${params.prefix}${field}` === this.sortField );
      if ( fieldTabParams?.fields?.length ){
        fieldTabParams.selected = {};
        fieldTabParams.fields.forEach(( field ) =>{
          fieldTabParams.selected[field] = false;
          console.log( visibleSortField );
          if ( !visibleSortField && `${params.prefix}${field}` === this.sortField ){
            params.visible_fields.push( field );
            this.vuesService.setViewParams$( this.viewParams );
            this.tableReloadService.emitReload$( `aggregate` );
          }
        });
      }
      console.log( params.visible_fields, fieldTabParams.fields );


      params.visible_fields.forEach(( field ) => {
        fieldTabParams.selected[field] = true;
      });

    }
  }
}
