import { Component, OnDestroy } from '@angular/core';
import { delay, of, Subject, takeUntil } from 'rxjs';
import { EnvService } from '../../services/env/env.service';

@Component({
  selector:    `alk-base`,
  standalone:  true,
  templateUrl: `./base.component.html`,
  styleUrls:   [`./base.component.scss`],
})
export abstract class BaseComponent implements OnDestroy {

  /** Vrai si le composant est en cours de chargement (pour afficher le contenu html uniquement à la fin du ngOnInit par exemple) */
  protected isLoading = false;
  /** Vrai si application est en mode debug (var d'env) */
  protected isInDebugMode = false;
  /** Subject émettant une valeur à la destruction du composant, à utiliser avec les pipes rxjs et **takeUntil(this.endSubscriptions)** sur les observable */
  protected endSubscriptions = new Subject<boolean>();
  /** En cas d'héritage multiple, permet aux enfants de préciser à leur parent de ne pas passer this.isLoading à **false** à la fin de leur *ngOnInit* */
  protected readonly keepLoadingAfterSuperInit: boolean = false;

  protected constructor() {
    this.isInDebugMode = EnvService.enableDebug ?? false;
  }

  ngOnDestroy(): void {
    this.endSubscriptions.next( true );
    this.endSubscriptions.complete();
  }

  protected reloadTable(){
    this.isLoading = true;
    of( this.isLoading ).pipe(
      takeUntil( this.endSubscriptions ),
      delay( 250 ),
    )
      .subscribe(() =>  this.isLoading = false );
  }
}
