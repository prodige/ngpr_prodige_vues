import { TestBed } from '@angular/core/testing';

import { BaseComponent } from './base.component';

describe('BaseComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ BaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
  });

  it('should create', () => {
    return true
  });
});
