import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { map } from 'rxjs';
import { UserService } from '../services/user.service';

export const isAdminGuard: CanActivateFn = () => {
  const userService = inject( UserService );
  return userService.getUserFromApi().pipe(
    map(( user )  => user.isAdmin ),
  );
};
