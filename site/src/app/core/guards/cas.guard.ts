import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { map } from 'rxjs';
import { CasService } from '../services/cas/cas.service';
import { EnvService } from '../services/env/env.service';

export const CasGuard: CanActivateFn = () => {
  const envService = inject( EnvService );
  const casService = inject( CasService );
  return casService.isConnected().pipe(
    map(( isConnected )  => {
      if ( !isConnected ){
        window.location.href = envService.casUrl + `/login?service=` + encodeURIComponent( window.location.href );
      }
      return isConnected;
    }));
};
