import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../env/env.service';
import { CasConnectApi } from '../../models/cas-connect-api';
import { catchError, delay, map, Observable, of, switchMap } from 'rxjs';

interface ErrorHttp {
  status: number
}

@Injectable({
  providedIn: `root`,
})
export class CasService {
  private isConnectedUser = false;

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
  ) {
  }

  public isConnected(): Observable<boolean> {
    return this.isConnectedUser ? of( this.isConnectedUser ) :
      this.httpClient.jsonp<CasConnectApi>( `${this.envService.catalogueUrl}/prodige/connect`, `callback` ).pipe(
        switchMap(() => this.httpClient.jsonp<CasConnectApi>( `${this.envService.urlVues}/prodige/connect`, `callback` )),
        switchMap(() => this.httpClient.jsonp<CasConnectApi>( `${this.envService.urlAdmin}/prodige/connect`, `callback` )),
        map(( casConnected ) =>  {

          this.isConnectedUser = casConnected.connected;
          return casConnected.connected;
        }),
        catchError(( err ) => {
          console.error( err );
          const httpError = <ErrorHttp>err;
          if ( httpError.status === 401 ){
            return of( false );
          }
          else {
            return of( false ).pipe( delay( 5000 ));
          }
        }),
      );
  }

  /**
   * Déconnexion du cas avec redirection vers un service
   * compatible avec prodige v5
   */
  logout(): void {
    window.location.href = `${this.envService.urlAdmin}/prodige/disconnectAll`;
  }
}
