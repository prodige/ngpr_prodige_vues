import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvService } from '../env/env.service';
import { firstValueFrom, map, Observable, of, ReplaySubject, throwError } from 'rxjs';
import { JSONLD_HEADER } from '../../models/jsonp-header';
import { ViewParams, ViewSort } from '../../models/view-params';
import { LoggerService } from '../../components/logger/logger.service';
import { LayerFieldService } from '../layer-field.service';
import { AdminService } from './admin.service';
import { JoinWarning } from '../../models/join-warning.model';
import { ProdigeField } from '../../models/prodige-field.model';
import { LAYER_TYPE } from '../../models/layer-type.model';
import { TViewType } from '../../models/view-type.model';

interface CREATE_VIEW {
  view_name:     string,
  resource:      string,
  dataset:       string,
  is_customized: boolean,
  view_sql?:      string,
}


@Injectable({
  providedIn: `root`,
})
export class VuesService {
  private readonly regexPrefix = /t\d*_(.*)?/g;
  private readonly regexPrefixCT = /\[(t\d*|c)_(.*)?]/g;
  private readonly regexExpression = /\[.*?]/g;

  private viewParams$ = new ReplaySubject<ViewParams>( 1 );
  private viewParam: ViewParams = null;

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
    private loggerService: LoggerService,
    private layerFieldService: LayerFieldService,
    private adminService: AdminService,
  ) {
  }

  getViewParams$( reload = false, uuid: string = null, type: TViewType = `with_assistant` ): Observable<ViewParams> {
    if ( !reload ) {
      return this.viewParams$.asObservable();
    }
    if ( reload && !uuid ) {
      return throwError(() => `NEED UUID FOR GET VIEW` );
    }
    const url = `${this.envService.urlVues}/api/view/${uuid}`;

    this.httpClient.get<ViewParams>( url, { withCredentials: true, headers: JSONLD_HEADER.headerGet })
      .subscribe({
        next: ( result ) => {
          switch ( type ) {
            case `with_assistant`:
              this.processWithAssistant( result );
              break;
            case `custom`:
              this.processCustom( result );
          }
        },
        error: ( error ) => {
          console.error( error );
          this.viewParams$.next( null );
        },
      });

    return this.viewParams$.asObservable();
  }

  patchViewParams$(): Observable<unknown> {
    this.visibleFieldCleaning();
    const url = `${this.envService.urlVues}/api/view/${this.viewParam.resource}`;

    const  fields = this.layerFieldService.getVisibleFieldWithPrefix( this.viewParam );
    this.aggregateCleaning( fields );

    let requestSql: ViewParams = null;
    if ( this.viewParam.view_sql ){
      requestSql = {
        view_sql:  this.viewParam.view_sql,
        view_name: this.viewParam.view_name,
        dataset:   this.viewParam.dataset,
        resource:  this.viewParam.resource,
      };
    }

    return this.httpClient.put<unknown>( url, requestSql ? requestSql : this.viewParam, {
      withCredentials: true,
      headers:         JSONLD_HEADER.headerPatch,
    });
  }

  patchCustomView$( viewParams: ViewParams, uuid: string ){
    const url = `${this.envService.urlVues}/api/view/${uuid}`;

    return firstValueFrom( this.httpClient.put<unknown>( url, viewParams, {
      withCredentials: true,
      headers:         JSONLD_HEADER.headerPatch,
    }));
  }

  private processWithAssistant( result: ViewParams ){
    if ( result.sort && !( result.sort instanceof Array )) {
      const sortTab: ViewSort[] = [];
      sortTab.push( result.sort );
      result.sort = sortTab;
    }

    if ( result.grouping_method && result.grouping_method instanceof Array ) {
      result.grouping_method = {};
    }
    result.prefix = `c_`;

    this.joinPrefixProcess( result );
    this.setViewParams$( result );
  }

  private processCustom( result: ViewParams ){
    if ( !result.view_sql ){
      result.view_sql = `SELECT * FROM public.${result.dataset}`;
    }

    this.setViewParams$({
      dataset:   result.dataset,
      resource:  result.resource,
      view_name: result.view_name,
      view_sql:  result.view_sql,
    });
  }

  postView( viewName: string, uuid: string, dataset: string, custom: boolean ): Observable<unknown>{
    const postData: CREATE_VIEW = {
      view_name:     viewName,
      resource:      uuid,
      dataset:       dataset,
      is_customized: custom,
    };

    if ( custom ){
      postData.view_sql = `SELECT * FROM public.${dataset}`;
    }

    return this.httpClient.post<unknown>( `${this.envService.urlVues}/api/view`, postData, {
      withCredentials: true,
      headers:         JSONLD_HEADER.headerPatch,
    });
  }

  setViewParams$( viewParams: ViewParams ) {
    this.viewParam = viewParams;
    this.viewParams$.next( viewParams );
  }

  getViewParams(): ViewParams {
    return this.viewParam;
  }

  resetView( uuid: string ){
    return this.httpClient.delete( `${this.envService.urlVues}/api/view/${uuid}` ).pipe(
      map(() => true ),
    );
  }

  onChangeJoins(): Observable<JoinWarning> {
    const errors = {
      inFilter:     false,
      inCalculated: false,
      inSort:       false,
    } as JoinWarning;
    console.log( `onChangeJoins : `, this.viewParam.joins );
    if ( this.viewParam?.joins instanceof Array ) {
      const layerRessources = this.adminService.getLayers( LAYER_TYPE.VECTOR );
      const layerTmp = layerRessources.find(( layer ) => layer.storagePath === this.viewParam.dataset );
      return this.layerFieldService.loadAllFieldView( this.viewParam, layerTmp.id ).pipe(
        map(( result ) => {
          console.log( ` onChangeJoins `, result );

          errors.inFilter = this.errorInFilter( result );
          errors.inCalculated = this.errorInCalculatedField( result );
          errors.inSort = this.errorInSort( result );

          if ( !errors.inSort && !errors.inFilter && !errors.inCalculated ){
            const  fields = this.layerFieldService.getVisibleFieldWithPrefix( this.viewParam );
            this.aggregateCleaning( fields );
            console.log( this.viewParam );
            this.setViewParams$( this.viewParam );
          }
          return errors;
        }),
      );
    } else {
      return of( errors );
    }
  }


  joinPrefixProcess( viewParams: ViewParams ) {
    if ( viewParams?.joins instanceof Array ) {
      viewParams.joins.forEach(( joinParam, idx ) => {
        if ( !joinParam.join_pos ) {
          joinParam.join_pos = idx;
        }
        const regex = new RegExp( this.regexPrefix );
        const found = regex.exec( joinParam.field_table );

        if ( found instanceof Array && found.length > 1 ) {
          joinParam.field_table = found[1];
        }

        joinParam.prefix = `t${joinParam.join_pos}_`;
      });
    }
  }

  /**
   * @return true si erreur, false sinon
   */
  private errorInCalculatedField( fields: ProdigeField[]) {
    if ( this.viewParam?.calculated_fields?.length && fields?.length ) {
      const epxressionWithError = this.viewParam.calculated_fields.find(( calcul ) => {
          const fieldExpressions = this.getFieldFromExpression( calcul.expression );

          const fieldFound = fieldExpressions.find(( field ) => {
            const isIn = fields.find(( usableField ) => `[${usableField.name}]` === field );
            return !isIn;
          });

          return !!fieldFound;
        },
      );

      return !!epxressionWithError;
    }

    return false;
  }

  /**
   * @return true si erreur, false sinon
   */
  private errorInFilter( fields: ProdigeField[]){
    if ( !this.viewParam?.filter ){
      return false;
    }
    const fieldExpressions = this.getFieldFromExpression( this.viewParam.filter );

    const fieldFound = fieldExpressions.find(( field ) => {
      const isIn = fields.find(( usableField ) => `[${usableField.name}]` === field );
      return !isIn;
    });

    return !!fieldFound;
  }

  private getFieldFromExpression( expression: string ){
    const regex = new RegExp( this.regexExpression );
    let fieldExpressions: string[] = [];
    let m;
    // on prend la valeur entre [ ]
    while (( m = regex.exec( expression )) !== null ) {
      if ( m.length ) {
        fieldExpressions.push( m[0]);
      }
    }

    // on filtres les valeurs commencent par un prefix tx_ ou c_
    fieldExpressions = fieldExpressions.filter(( field ) => {
      if ( !field ) {
        return false;
      }
      const regex = new RegExp( this.regexPrefixCT );
      const found = regex.exec( field );
      return ( found instanceof Array && found.length > 1 );
    });

    return fieldExpressions;
  }

  private errorInSort( fields: ProdigeField[]){
    if ( !this.viewParam?.sort?.length ){
      return false;
    }

    const sortFound = this.viewParam.sort.find(( sort ) =>
      fields.find(( field ) => field.name === sort.field ),
    );

    return !sortFound;
  }

  private aggregateCleaning( fields: ProdigeField[]){
    console.log( ` aggregateCleaning `, this.viewParam?.grouping_method );
    if ( this.viewParam?.grouping_fields?.length ){
      this.viewParam.grouping_fields = this.viewParam.grouping_fields.filter(( field ) =>
        fields.find(( prodigeField ) => prodigeField.name === field ),
      );
    }

    if ( this.viewParam?.grouping_method ){
      const size = Object.values( this.viewParam.grouping_method )?.length;
      if ( !size ){
        this.viewParam.grouping_fields = [];
      }
      console.log( `VIEW PARAMS `, this.viewParam );
      const fieldKeys = Object.keys( this.viewParam.grouping_method );
      fieldKeys.forEach(( fieldKeys ) => {
        const found = fields.find(( field ) => field.name === fieldKeys );
        console.log( `aggregateCleaning `, found, fields );
        if (( !found && fieldKeys !== `the_geom` ) || !this.viewParam.grouping_method[fieldKeys]){
          delete this.viewParam.grouping_method[fieldKeys];
        }
      });
    }
  }

  private visibleFieldCleaning(){
    if ( this.viewParam?.visible_fields?.length ){
      this.viewParam.visible_fields = this.viewParam.visible_fields.filter(( field ) => field  !== `the_geom` );
    }
  }

  clearViewParms(){
    this.viewParams$ = new ReplaySubject<ViewParams>( 1 );
  }
}
