import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AsyncSubject, map, Observable, of } from 'rxjs';
import { EnvService } from '../env/env.service';
import { LayerDataset, LayerResource } from '../../models/layer-resource';
import { JSONLD_HEADER } from '../../models/jsonp-header';
import { LayerField } from '../../models/layer-field';
import { IRightApi } from '../../models/user-right.model';


interface DatasetAPi{
  layers: LayerDataset[];
}

@Injectable({
  providedIn: `root`,
})
export class AdminService {
  private layers: { [key: number]: LayerResource[]} = {};
  private layersAsync: {[key: number]: AsyncSubject<LayerResource[]>} = {};

  private layerWithUuid: {[key: number]: LayerResource} = {};

  constructor(
    private httpClient: HttpClient,
    private envService: EnvService,
  ) {
  }

  getLayers( layerType: number ): LayerResource[] {
    console.log( `this.layers`, this.layers, layerType );
    return this.layers[layerType] && this.layers[layerType] instanceof Array ? this.layers[layerType] : [];
  }

  getLayers$( layerType: number ): Observable<LayerResource[]> {
    if ( this.layers[layerType]){
      return of( this.layers[layerType]);
    }

    if ( this.layersAsync[layerType]){
      return this.layersAsync[layerType].asObservable();
    }

    this.layersAsync[layerType] = new AsyncSubject<LayerResource[]>();
    const url = `${this.envService.urlAdmin}/api/view/resource/dataset/${layerType}`;

    this.httpClient.get<DatasetAPi>( url, {
      withCredentials: true,
    })
      .pipe(
        map(( result ) => result?.layers?.length ? result.layers : []),
        map(( layers ) => {
          // conversion vers le format LayerResource
          this.layers[layerType] = layers.map(( layer ) =>
            ({
                id:           layer.layer_id,
                name:         layer.name,
                storagePath:  layer.storage_path,
                metadataUuid: layer.uuid,
              } as LayerResource
            ));

        }),
      )
      .subscribe({
        next: () => {
          this.layersAsync[layerType].next( this.layers[layerType]);
          this.layersAsync[layerType].complete();
        },
        error: (( err ) =>{
          console.error( err );
          this.layersAsync[layerType].complete();
        }),
      });

    return  this.layersAsync[layerType].asObservable();
  }

  getField$( layerId: number ): Observable<LayerField>{
    const url = `${this.envService.urlAdmin}/api/layers/${layerId}/fields`;
    return this.httpClient.get<LayerField>( url, { withCredentials: true, headers: JSONLD_HEADER.headerGet });
  }

  getLayer$( layerId: number ): Observable<LayerResource>{
    if ( this.layerWithUuid[layerId]){
      return of( this.layerWithUuid[layerId]);
    }

    const url = `${this.envService.urlAdmin}/api/layers/${layerId}`;
    return this.httpClient.get<LayerResource>( url, { withCredentials: true, headers: JSONLD_HEADER.headerGet }).pipe(
      map(( result ) => {
       if ( !result ){
          throw new Error( `layer null` );
       }
       const layer = {
         id:           result.id,
         name:         result.name,
         storagePath:  result.storagePath,
         metadataUuid: result.metadataUuid,
       } as LayerResource;

       this.layerWithUuid[layerId] = layer;

       return layer;
      }),
    );
  }

  getUserRight$(): Observable<IRightApi>{
    return this.httpClient.get<IRightApi>( `${this.envService.urlAdmin}/api/internal/verify_right?TRAITEMENTS=GLOBAL_RIGHTS`, { withCredentials: true });
  }

}
