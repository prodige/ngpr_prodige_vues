import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AsyncSubject, Observable, of } from 'rxjs';
import { LoggerService } from '../components/logger/logger.service';

/**
 * Service abstract, pour charger des données http une seul fois avec validation zod
 * Ne pas modifier le constructor
 */
@Injectable({
  providedIn: `root`,
})
export class GetDataService<T> {

  private data: T = null;
  private dataAsync: AsyncSubject<T>;

  protected constructor(
    protected httpClient: HttpClient,
    protected loggerService?: LoggerService,
  ) {
  }

  /**
   * pour charger des données http une seul fois avec validation zod
   */
  public getData$( url: string, messageErr?: string,
                     option?: unknown, isJsonp: boolean = false ): Observable<T> {
    if ( this.data ) {
      return of( this.data );
    }

    if ( this.dataAsync && this.dataAsync.observed ) {
      return this.dataAsync.asObservable();
    }

    this.dataAsync = new AsyncSubject<T>();
    const request = isJsonp ? this.httpClient.jsonp<T>( url, `callback` ) : this.httpClient.get<T>( url, option );
    request.subscribe({
      next: ( data: T ) => {
        this.data = data;
        this.dataAsync.next( this.data );
        this.dataAsync.complete();
      },
      error: ( err ) => {
        console.error( err );
        if ( this.loggerService && messageErr ) {
          this.loggerService.errorOnToast$( messageErr );
        }
        this.dataAsync.complete();
      },
    });

    return this.dataAsync.asObservable();
  }
}
