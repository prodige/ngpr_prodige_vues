import { Injectable } from '@angular/core';
import { AdminService } from './prodige-api/admin.service';
import { LAYER_TYPE } from '../models/layer-type.model';
import { ViewParams } from '../models/view-params';
import { LayerResource } from '../models/layer-resource';
import { BehaviorSubject, catchError, firstValueFrom, map, of, switchMap } from 'rxjs';
import { VuesService } from './prodige-api/vues.service';
import { LoggerService } from '../components/logger/logger.service';
import { Router } from '@angular/router';
import { TViewType } from '../models/view-type.model';

@Injectable({
  providedIn: `root`,
})
export class HomeService {
  protected viewParams$ = new BehaviorSubject<ViewParams>( null );

  protected layerRessources$ = new BehaviorSubject<LayerResource[]>([]);

  // Layer utilisé par la vue
  protected layerSource$ = new BehaviorSubject<LayerResource>( null );

  constructor(
    private adminService: AdminService,
    private vueService: VuesService,
    private loggerService: LoggerService,
    private router: Router,
  )
  { }

  public getViewParams$() { return this.viewParams$.asObservable() }

  public getLayerSource$() { return this.layerSource$.asObservable() }

  public async saveView() {
    return await firstValueFrom( this.vueService.patchViewParams$());
  }

  public loadToView( uuid: string, type: TViewType = `with_assistant` ){
    this.loadViewParams$( uuid, type ).pipe(
      switchMap(( result ) => result ? this.loadLayers$() : of( null )),
    )
      .subscribe(() =>{
        const layerRessource = this.layerRessources$.getValue();
        const viewparams = this.viewParams$.getValue();

        if ( !layerRessource?.length || !viewparams ){
          return;
        }

        const layerSource = layerRessource.find(( layer ) => layer.storagePath === viewparams.dataset );

        this.layerSource$.next( layerSource );
      });
  }

  public resetView(){
    const viewParam = this.viewParams$.getValue();
    let layerView: LayerResource = null;

    this.adminService.getLayers$( LAYER_TYPE.VIEW ).pipe(
      switchMap(( layerResource ) => {
        layerView = layerResource.find(( layer ) => layer.metadataUuid === viewParam.resource );
        return this.vueService.resetView( viewParam.resource );
      }),
      map(( isOk ) => {
        if ( !isOk ){
          return;
        }
        this.clean();
        this.router.navigate([ `/create`, viewParam.resource, layerView.id ]).catch( console.error );
      }),
    )
      .subscribe();
  }

  private clean(){
    this.viewParams$.next( null );
    this.layerRessources$.next([]);
    this.layerSource$.next( null );

    this.vueService.clearViewParms();
  }

  private loadLayers$(){
    return this.adminService.getLayers$( LAYER_TYPE.VECTOR  ).pipe(
      switchMap(( result )  => {
        this.layerRessources$.next( result );
        return of( true );
      }),
      catchError(( err ) =>{
        console.error( err );
        this.loggerService.errorOnToast$( `Erreur survenue pendant le chargement des données` );
        return of( false );
      }),
    );
  }

  private loadViewParams$( uuid: string, type: TViewType = `with_assistant` ){
    return this.vueService.getViewParams$( true, uuid, type ).pipe(
      switchMap(( result )  => {
        this.viewParams$.next( result );
        return of( true );
      }),
      catchError(( err ) =>{
        console.error( err );
        return of( false );
      }),
    );
  }

}
