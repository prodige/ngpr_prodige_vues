import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


export type ReloadType = `aggregate`
/**
 * Service utilisé pour savoir quand recharge l'affichage des tableaux
 */
@Injectable({
  providedIn: `root`,
})
export class TableReloadService {
  private reload$ = new Subject<boolean>();
  private reloadAggregate$ = new Subject<boolean>();

  emitReload$( type?: ReloadType ){
    this.reloadAggregate$.next( true );
    if ( !type ){
      this.reload$.next( true );
    }
  }

  canReload$( type?: ReloadType ): Observable<boolean>{
    if ( type && type === `aggregate` ){
      return this.reloadAggregate$.asObservable();
    }
    return this.reload$.asObservable();
  }
}
