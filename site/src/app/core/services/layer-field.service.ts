import { Injectable } from '@angular/core';
import { LayerField } from '../models/layer-field';
import { AsyncSubject, from, map, Observable, of, switchMap } from 'rxjs';
import { LoggerService } from '../components/logger/logger.service';
import { AdminService } from './prodige-api/admin.service';
import { ViewParams } from '../models/view-params';
import { LayerResource } from '../models/layer-resource';
import { FieldTabParams } from '../models/field-tab-params';
import { ProdigeField } from '../models/prodige-field.model';
import { LAYER_TYPE } from '../models/layer-type.model';

@Injectable({
  providedIn: `root`,
})
export class LayerFieldService {
  private layerFields: { [key: number]: LayerField } = {};
  private dataAsync: { [key: number]: AsyncSubject<LayerField> } = {};

  protected constructor(
    private loggerService?: LoggerService,
    private adminService?: AdminService,
  ) {
  }

  /**
   * pour charger des données http une seul fois
   */
  public getLayerField$( layerId: number ): Observable<LayerField> {
    if ( this.layerFields[layerId]) {
      return of( this.layerFields[layerId]);
    }

    if ( this.dataAsync[layerId] && this.dataAsync[layerId].observed ) {
      return this.dataAsync[layerId].asObservable();
    }

    this.dataAsync[layerId] = new AsyncSubject<LayerField>();
    this.adminService.getField$( layerId ).subscribe({
      next: ( data ) => {
        this.layerFields[layerId] = data;
        if ( this.layerFields[layerId]?.dbFields[`the_geom`]) {
          delete this.layerFields[layerId].dbFields[`the_geom`];
        }
        this.dataAsync[layerId].next( this.layerFields[layerId]);
        this.dataAsync[layerId].complete();
      },
      error: ( err ) => {
        console.error( err );
        this.dataAsync[layerId].complete();
      },
    });

    return this.dataAsync[layerId].asObservable();
  }

  /**
   * Pour tous les champs utilisables d'une vue, y compris ceux des jointures
   */
  loadAllFieldView( viewParams: ViewParams, layerId: number ): Observable<ProdigeField[]> {
    let fieldsView: ProdigeField[] = [];
    return this.getLayerField$( layerId ).pipe(
      switchMap(( layersField ) => {
        // les champs de la table principal comment par c_
        const fieldsTmp = layersField && layersField.dbFields ? Object.entries( layersField.dbFields ) : [];
        fieldsView = fieldsTmp.map(( fieldsTmp ) =>({ name: fieldsTmp[0], type: fieldsTmp[1] }));
        fieldsView.forEach(( fieldsView ) =>{
          fieldsView.name = `c_${fieldsView.name}`;
        });

        if ( viewParams?.joins?.length ) {
          viewParams.joins.forEach(( joinParams, idx ) => {
            joinParams.join_pos = joinParams.join_pos ? joinParams.join_pos : idx;
          });
        }

        // pour les champs des autres jointure
        if ( viewParams?.joins?.length ){
          return from( viewParams?.joins );
        }
        return of( null );

      }),
      switchMap(( joinParam ) => {
        let layer: LayerResource = null;
        if ( joinParam ) {
          layer = this.adminService.getLayers( LAYER_TYPE.TABULAIRE ).find(
            ( layerRessource ) => layerRessource.storagePath === joinParam.dataset,
          );
        }
        if ( layer ) {
          return this.getLayerField$( layer.id )
            .pipe(
              map(( fields ) => ({ fields, joinParam })),
            );
        }

        return of( null );
      }),
      map((( result ) => {
        if ( result ) {
          const layersField = result.fields;

          // les champs des jointures commence par tx_ x position de la jointure
          const fieldTemps = layersField && layersField.dbFields ? Object.entries( layersField.dbFields ) : [];
          const fieldJoins: ProdigeField[] = fieldTemps.map(( fieldsTmp ) =>({ name: fieldsTmp[0], type: fieldsTmp[1] }));
          fieldJoins.forEach(( fieldJoin )=> {
            fieldJoin.name = `${result.joinParam.prefix}${ fieldJoin.name}`;
          });
          fieldsView = fieldsView.concat( fieldJoins );
        }
        return fieldsView;
      })),
    );
  }


  getVisibleFieldWithPrefix( viewParams: ViewParams ):ProdigeField[]{
    let fieldsView: ProdigeField[] = [];

    if ( viewParams && viewParams.visible_fields?.length ){
      fieldsView = viewParams.visible_fields.map(( field ) => ({ name: `${viewParams.prefix}${field}` }));
    }

    if ( viewParams && viewParams.joins?.length ){
       viewParams.joins.forEach(( joinParams ) => {
         if ( joinParams.visible_fields?.length ){
           const tabTmp = joinParams.visible_fields.map(( field ) => ({ name: `${joinParams.prefix}${field}` }));
           fieldsView = fieldsView.concat( tabTmp );
         }
       });
    }

    return fieldsView;
  }

  buildTabs$( viewParams: ViewParams, withPrefix: boolean = false ): Observable<{ active: FieldTabParams, fieldTabParams: FieldTabParams[] }> {
    const fieldTabParams: FieldTabParams[] = [];
    let active: FieldTabParams = null;
    const layerRessources = this.adminService.getLayers( LAYER_TYPE.VECTOR );
    console.log( `-----` );
    let layerTabulaires: LayerResource[];
    console.log( `layerTabulaires `, LAYER_TYPE.TABULAIRE, layerTabulaires );
    if ( !layerRessources?.length ) {
      return of({ active, fieldTabParams });
    }

    const layerTmp = layerRessources.find(( layer ) => layer.storagePath === viewParams.dataset );

    // on commence par récupérer les champs de la vue
    return this.adminService.getLayers$( LAYER_TYPE.TABULAIRE ).pipe(
      switchMap(( layerTab ) => {
        layerTabulaires = layerTab;
        return this.getLayerField$( layerTmp.id );
      }),
      switchMap(( fields ) => {
        const dbFields = fields?.dbFields ? fields?.dbFields : {};

        active = {
          geomFunction: viewParams.grouping_method[`the_geom`] ? viewParams.grouping_method[`the_geom`] : null,
          withGeom:     !!Object.keys( dbFields ).find(( field ) => field === `the_geom` ),
          dataset:      viewParams.dataset,
          prefix:       viewParams.prefix,
          fields:       Object.keys( dbFields )
            .filter(( field ) => field != `the_geom` ),

        };

        if ( withPrefix ) {
          active.fields = active.fields.map(( field ) => `c_${field}` );
        }

        fieldTabParams.push( active );

        // on charge les champs des jointures
        return viewParams?.joins?.length ? from( viewParams?.joins ) : of( null );
      }),
      switchMap(( joinParam ) => {
        let layer: LayerResource = null;

        if ( joinParam ) {
          layer = layerTabulaires.find(( layer ) => layer.storagePath === joinParam.dataset );
        }

        if ( layer ) {
          return this.getLayerField$( layer.id )
            .pipe(
              map(( fields ) => ({ fields, joinParam })),
            );
        }

        return of( null );
      }),
      map(( result ) => {
          if ( !result ) {
            return { active, fieldTabParams };
          }
          const joinParams = result.joinParam;
          const fields = result.fields;
          const dbFields = fields?.dbFields ? fields?.dbFields : {};



          const fieldTabParam = {
            geomFunction: viewParams.grouping_method[`the_geom`] ? viewParams.grouping_method[`the_geom`] : null,
            withGeom:     !!Object.keys( dbFields ).find(( field ) => field === `the_geom` ),
            dataset:      joinParams.dataset,
            joinParams:   joinParams,
            prefix:       joinParams.prefix,
            fields:       Object.keys( dbFields )
              .filter(( field ) => field != `the_geom` ),
          };
          if ( withPrefix ) {
            fieldTabParam.fields = fieldTabParam.fields.map(( field ) => `${joinParams.prefix}${field}` );
          }

          fieldTabParams.push( fieldTabParam );


          return { active, fieldTabParams };
        },
      ),
    );

  }

  buildAggregatTabs( viewParams: ViewParams, withPrefix: boolean = false ): { active: FieldTabParams, fieldTabParams: FieldTabParams[] }{
    if ( !viewParams || !viewParams.visible_fields?.length ){
      return null;
    }
    const fieldTabParams: FieldTabParams[] = [];
    const fields = viewParams.visible_fields;
    const active = {
      geomFunction: viewParams.grouping_method[`the_geom`] ? viewParams.grouping_method[`the_geom`] : null,
      withGeom:     !!fields.find(( field ) => field === `the_geom` ),
      dataset:      viewParams.dataset,
      prefix:       viewParams.prefix,
      fields:       fields.filter(( field ) => field != `the_geom` ),

    };

    viewParams.visible_fields.forEach(( field ) => {
      const found = viewParams.grouping_fields.find(( grpField ) => grpField === `${viewParams.prefix}${field}` );

      if ( !found && !viewParams.grouping_method[`${viewParams.prefix}${field}`]){
        viewParams.grouping_fields.push( `${viewParams.prefix}${field}` );
      }
      console.log( `found `, found, field, viewParams );
    });
    if ( withPrefix ) {
      active.fields = active.fields.map(( field ) => `c_${field}` );
    }

    fieldTabParams.push( active );

    if ( !viewParams.joins?.length ){
      return { active, fieldTabParams };
    }

    viewParams.joins.forEach(( joinParams ) => {
      const fields = joinParams.visible_fields;
      const fieldTab = {
        dataset:      joinParams.dataset,
        joinParams:   joinParams,
        prefix:       joinParams.prefix,
        fields:       fields.filter(( field ) => field != `the_geom` ),

      };
      if ( withPrefix ) {
        fieldTab.fields = fieldTab.fields.map(( field ) => `${joinParams.prefix}${field}` );
      }
      fieldTabParams.push( fieldTab );
    });

    return { active, fieldTabParams };
  }
}
