import { Injectable } from '@angular/core';

@Injectable({
  providedIn: `root`,
})
export class EnvService {
  static enableDebug = false;

  /** Contribution */
  public urlContribution = `https://contribution.prodige.internal`;

  /** Server Carto **/
  public serverUrl =  `https://carto.prodige.internal`;

  /** Catalogue */
  public catalogueUrl =  `https://catalogue.prodige.internal`;

  /** Cas */
  public casUrl = `https://cas.prodige.internal`;

  /** Adminc carto */
  public admincartoUrl = `https://admincarto.prodige.internal`;

  /** Datacarto */
  public datacarto = `https://datacarto.prodige.internal`;

  /** Datacarto */
  public urlAdmin = `https://admin.prodige.internal`;

  /** Vues */
  public urlVues = `https://vues.prodige.internal`;

  public availableProjections = {
    2154: {
      key:    `EPSG:2154`,
      label:  `RGF93 / Lambert 93`,
      proj:   `+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs`,
      bounds: [ -357823.2365, 6037008.6939, 1313632.3628, 7230727.3772 ],
    },
    3857:  {
      key:    `EPSG:3857`,
      label:  `WGS 84 / Pseudo-Mercator`,
      proj:   `+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs`,
      bounds: [ -20026376.39, -20048966.10, 20026376.39, 20048966.10 ],
    },
    4326:  {
      key:    `EPSG:4326`,
      label:  `WGS84`,
      proj:   `+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs`,
      bounds: [ -180.0000, -90.0000, 180.0000, 90.0000 ],
    },
  };
}

