import { EnvService } from './env.service';

export const EnvServiceFactory = () => {
  // Create env
  const envTmp :{ [key: string]: unknown } = {};

  // Read environment variables from browser window
  const browserWindow = ( window as unknown as { [key: string]: unknown }) || {};
  const browserWindowEnv = ( browserWindow[`__env`] as { [key: string]: unknown }) || {};

  // Assign environment variables from browser window to env
  // In the current implementation, properties from env.js overwrite defaults from the EnvCoreService.
  // If needed, a deep merge can be performed here to merge properties instead of overwriting them.
  for ( const key in browserWindowEnv ) {
    if ( browserWindowEnv.hasOwnProperty( key )) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      envTmp[key] = ( browserWindow[`__env`] as { [key: string]: unknown })[key];
    }
  }

  const serviceTmp = new EnvService();
  Object.assign( serviceTmp, envTmp );

  return serviceTmp;
};


export const EnvServiceProvider = {
  provide:    EnvService,
  useFactory: EnvServiceFactory,
  deps:       <unknown[]>[],
};

