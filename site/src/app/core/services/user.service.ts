import { Injectable } from '@angular/core';
import { AdminService } from './prodige-api/admin.service';
import { BehaviorSubject, map } from 'rxjs';
import { IRightApi, IUserRight } from '../models/user-right.model';


@Injectable({
  providedIn: `root`,
})
export class UserService {
  private user$ = new BehaviorSubject<IUserRight>( null );

  constructor(
    private adminService: AdminService,

  ) {}

  getUser$(){
    return this.user$.asObservable();
  }

  loadUser(){
    if ( this.user$.value ){
      return;
    }

    this.adminService.getUserRight$().subscribe({
       next: ( result ) =>{
         const user = this.processUser( result );
         this.user$.next( user );
       },
    });
  }

  getUserFromApi(){
    return this.adminService.getUserRight$().pipe(
      map(( result ) =>{
        const user = this.processUser( result );
        this.user$.next( user );
        return user;
      }),
    );
  }

  processUser( userApi: IRightApi ){
    return  {
      id:        userApi.right?.userId,
      email:     userApi?.right?.userEmail,
      firstname: userApi?.right?.userPrenom,
      lastname:  userApi?.right?.userNom,
      isAdmin:   userApi?.right?.ADMINISTRATION,
      login:     userApi?.right?.userLogin,
    } as IUserRight;
  }
}
