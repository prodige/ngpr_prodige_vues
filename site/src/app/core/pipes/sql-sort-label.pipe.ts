import { Pipe, PipeTransform } from '@angular/core';
import { SQL_SORT_LABELS } from '../models/constant-sql';

@Pipe({
  name:       `sqlSortLabel`,
  standalone: true,
})
export class SqlSortLabelPipe implements PipeTransform {

  transform( value: string ): string {
    const sqlSortLabel = SQL_SORT_LABELS.find(
      ( sqlLabel ) => value === sqlLabel.value );

    return sqlSortLabel  ? sqlSortLabel.label : value;
  }

}
