import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name:       `booleanText`,
  standalone: true,
})
export class BooleanTextPipe implements PipeTransform {

  transform( value: boolean ): string {
    return value ? $localize`:@@yes:oui` : $localize`:@@no:non`;
  }

}
