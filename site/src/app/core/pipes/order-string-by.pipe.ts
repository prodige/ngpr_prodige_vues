import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name:       `OrderStringBy`,
  standalone: true,
})
export class OrderStringByPipePipe implements PipeTransform {

  transform<T extends {[key: string]: unknown} >( values: T[], field: string ): T[] {
    if ( !values?.length ){
      return values;
    }
    return  values.sort(( a, b ) =>
      ( <string>a[field]).localeCompare( <string>b[field]),
    );
  }

}
