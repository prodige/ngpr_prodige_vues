import { Pipe, PipeTransform } from '@angular/core';
import { SQL_TYPE } from '../models/constant-sql';

@Pipe({
  name:       `sqlTypeLabel`,
  standalone: true,
})
export class SqlTypeLabelPipe implements PipeTransform {

  transform( value: string ): string {
    const sqlTypeLabel = SQL_TYPE.find(
      ( sqlLabel ) => value === sqlLabel.value );

    return sqlTypeLabel  ? sqlTypeLabel.label : value;
  }
}
