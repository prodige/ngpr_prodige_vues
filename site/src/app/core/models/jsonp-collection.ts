export interface JsonpCollection<T> {
  "hydra:member": T[]
}
