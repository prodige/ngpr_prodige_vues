export interface CalculatedField {
  name: string;
  type: string;
  expression: string;
}

