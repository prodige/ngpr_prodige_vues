export interface LayerField {
  id: number,
  name: number,
  dbFields: {[key: string]: string},
}
