export const LAYER_TYPE = {
  VECTOR:    1,
  RASTER:    2,
  TABULAIRE: 3,
  VIEW:      4,
} as const;
