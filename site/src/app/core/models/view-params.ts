import { JoinParams } from './join-params';
import { CalculatedField } from './calculated-field';

export interface ViewSort {
  field: string;
  method: string;
}
export interface ViewParams {
  view_name?: string;
  resource?: string;
  dataset?: string;
  joins?: JoinParams[];
  calculated_fields?: CalculatedField[];
  visible_fields?: string[];
  grouping_fields?: string[];
  grouping_method?: {[key: string]: string}
  filter?: string;
  sort?: ViewSort[];
  prefix?: string;
  view_sql?: string;
  is_customized?: boolean;
}
