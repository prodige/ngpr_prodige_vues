export type LayerResource = {
  id: number;
  name: string;
  storagePath: string;
  metadataUuid?: string;
}

export interface LayerDataset{
  layer_id: number
  name: string
  storage_path: string
  uuid: string
}
