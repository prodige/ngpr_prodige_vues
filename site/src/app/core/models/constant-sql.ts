/**
 * Opérateur sql repris de prodige 4
 */
export const SQL_OPERATOR = [
  [ `+`, `+`, `addition`, `math`, `(number,number,number)` ],
  [ `-`, `-`, `soustraction`, `math`, `(number,number,number)` ],
  [ `*`, `*`, `multiplication`, `math`, `(number,number,number)` ],
  [ `/`, `/`, `division`, `math`, `(number,number,number)` ],
  [ `%`, `%`, `modulo`, `math`, `(number,number,number)` ],
  [ `^`, `^`, `exposant`, `math`, `(number,number,number)` ],
  [ `sqrt()`, `sqrt()`, `racine carrée`, `math`, `(number,number)` ],
  [ `abs()`, `abs()`, `valeur absolue`, `math`, `(number,number)` ],
  [ `round()`, `round()`, `arrondi`, `math`, `(number,number)` ],
  [ `>`, `>`, `supérieur à`, `math`, `(number,number,bool)` ],
  [ `>=`, `>=`, `supérieur ou égal à`, `math`, `(number,number,bool)` ],
  [ `<`, `<`, `inférieur à`, `math`, `(number,number,bool)` ],
  [ `<=`, `<=`, `inférieur ou égal à`, `math`, `(number,number,bool)` ],

  [ `()`, `()`, `parenthèses`, `log`, `(A,A)` ],
  [ `AND`, `AND`, `et logique`, `log`, `(bool,bool,bool)` ],
  [ `OR`, `OR`, `ou logique`, `log`, `(bool,bool,bool)` ],
  [ `NOT`, `NOT`, `non logique`, `log`, `(bool,bool)` ],
  [ `=`, `=`, `égalité`, `log`, `(A,A,A)` ],
  [ `!=`, `<>`, `différence`, `log`, `(A,A,A)` ],

  [ `concat`, `||`, `concaténation de chaînes de caractères`, `string`, `(string,string,string)` ],
  [ `like`, `like`, `comparaison de chaînes de caractères`, `string`, `(string,string,bool)` ],
  [ `ilike`, `ilike`, `comparaison de chaînes de caractères (insensible à la casse)`, `string`, `(string,string,bool)` ],
] as const;

export const SQL_TYPE = [{
    label: `Entier`,
    value: `int4`,
  }, {
    label: `Réel`,
    value: `float8`,
  }, {
    label: `Chaîne de caractères`,
    value: `varchar`,
  }, {
    label: `Booléen`,
    value: `boolean`,
  },
] as const;

export const AGGREGATE_FUNCTION = [
  {
    label: `count`,
    value: `count`,
  }, {
    label: `sum`,
    value: `sum`,
  }, {
    label: `avg (moyenne)`,
    value: `avg`,
  }, {
    label: `min`,
    value: `min`,
  }, {
    label: `max`,
    value: `max`,
  }, {
    label: `string_agg (concaténation)`,
    value: `string_agg`,
  },
] as const;


export const SQL_SORT_LABELS = [{
  label: `Ascendant`,
  value: `asc`,
}, {
  label: `Descendant`,
  value: `desc`,
}] as const;
