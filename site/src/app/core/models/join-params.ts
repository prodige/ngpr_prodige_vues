export interface JoinParams {
  resource: string;
  dataset: string;
  field_view: string;
  field_table: string;
  restrictive: boolean;
  visible_fields: string[];
  join_pos: number;
  prefix: string;
}
