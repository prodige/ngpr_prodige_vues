export interface JoinWarning {
  inFilter:     boolean;
  inCalculated: boolean;
  inSort:       boolean;
}

