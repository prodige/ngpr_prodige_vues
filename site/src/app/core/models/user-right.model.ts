export interface IRightApi{
  right: IUserRightApi;
}

export interface IUserRightApi {
  userNom: string;
  userPrenom: string;
  userLogin: string;
  userId: number;
  userEmail: string;
  ADMINISTRATION: boolean
}

export interface IUserRight{
  id: number
  firstname: string
  lastname: string
  login: string
  email: string
  isAdmin: boolean
}
