export interface CasConnectApi{
  success:  boolean;
  connected: boolean;
  login: string;
}
