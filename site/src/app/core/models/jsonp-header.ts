export const JSONLD_HEADER  = {
  headerGetCollection: { 'Accept': `application/ld+json` },
  headerGet:           { 'Accept': `application/ld+json`, 'Content-Type': `application/ld+json` },
  headerPatch:         { 'Accept': `application/ld+json`, 'Content-Type':  `application/merge-patch+json` },
};
