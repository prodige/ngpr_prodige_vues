export interface AlkGeojson {
  type: string
  features: AlkFeature[]
}

export interface AlkFeature {
  type: string
  geometry: AlkGeometry
  properties: AlkProperties
}

export interface AlkGeometry {
  type: string
  crs: AlkCrs
  coordinates: number[][]
}

export interface AlkCrs {
  type: string
  properties: AlkProperties
}

export interface AlkProperties {
  [key:string]: string;
}
