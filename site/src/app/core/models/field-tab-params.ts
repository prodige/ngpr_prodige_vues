export interface FieldTabParams {
  dataset: string;
  fields: string[];
  geomFunction?: string;
  prefix:    string;
  withGeom?: boolean;
  selected?: {[key: string]:boolean};
}
