import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { ViewParams } from '../../core/models/view-params';
import { LayerResource } from '../../core/models/layer-resource';
import { JoinModalComponent } from '../../core/components/join-modal/join-modal.component';
import { CommonModule } from '@angular/common';
import { JoinListComponent } from '../../core/components/join-list/join-list.component';
import {
  CalculatedFieldModalComponent,
} from '../../core/components/calculated-field-modal/calculated-field-modal.component';
import { CalculatedFieldsComponent } from '../../core/components/calculated-fields/calculated-fields.component';
import { FieldVisibilityComponent } from '../../core/components/field-visibility/field-visibility.component';
import { ParameterAggregateComponent } from '../../core/components/parameter-aggregate/parameter-aggregate.component';
import { BaseComponent } from '../../core/components/base/base.component';
import { ViewFilterModalComponent } from '../../core/components/view-filter-modal/view-filter-modal.component';
import { ViewFilterComponent } from '../../core/components/view-filter/view-filter.component';
import { map, Observable } from 'rxjs';
import { SortingDataListComponent } from '../../core/components/sorting-data-list/sorting-data-list.component';
import { SortingDataModalComponent } from '../../core/components/sorting-data-modal/sorting-data-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmComponent } from '../../core/components/modal-confirm/modal-confirm.component';
import { PreviewModalComponent } from '../../core/components/preview-modal/preview-modal.component';
import { HomeService } from '../../core/services/home.service';
import { LoggerService } from '../../core/components/logger/logger.service';
import { HeaderComponent } from '../../core/components/header/header.component';

@Component({
  standalone:  true,
  selector:    `alk-home`,
  templateUrl: `./manage.component.html`,
  imports:     [
    CommonModule,
    JoinModalComponent,
    JoinListComponent,
    CalculatedFieldModalComponent,
    CalculatedFieldsComponent,
    FieldVisibilityComponent,
    ParameterAggregateComponent,
    ViewFilterModalComponent,
    ViewFilterComponent,
    SortingDataListComponent,
    SortingDataModalComponent,
    RouterLink,
    PreviewModalComponent,
    HeaderComponent,
  ],
})
export class ManageComponent extends BaseComponent implements OnInit {
  protected viewParams$: Observable<ViewParams>;

  protected layerSource$: Observable<LayerResource>;

  protected activatedRoute: ActivatedRoute;
  protected router: Router;
  protected modalService: NgbModal;
  protected homeService: HomeService;
  protected loggerService: LoggerService;
  constructor(

  ) {
    super();

    this.activatedRoute = inject( ActivatedRoute );
    this.router = inject( Router );
    this.modalService = inject( NgbModal );
    this.homeService = inject( HomeService );
    this.loggerService = inject( LoggerService );

    this.viewParams$ = this.homeService.getViewParams$().pipe(
      map(( viewParams ) =>{
        if ( viewParams?.is_customized ){
          this.router.navigate([  `custom`, viewParams.resource ]).catch( console.error );
        }

        return viewParams;
      }),
    );
    this.layerSource$ = this.homeService.getLayerSource$();
  }

  ngOnInit() {

    const uuid = this.activatedRoute.snapshot.paramMap.get( `uuid` );

    if ( uuid ){
      this.homeService.loadToView( uuid );
    }
  }

  onReset(){
    const modalRef = this.modalService.open( ModalConfirmComponent );
    const insance =  <ModalConfirmComponent>modalRef.componentInstance;

    insance.message = `Cette réinitialisation entraînera la suppression `
                    + `de la vue en base de données et la suppression de `
                    + `sa présence dans les cartes. Souhaitez-vous poursuivre la réinitialisation ?`;

    insance.withCancelButton = true;

    modalRef.result
      .then(( confirm ) => {
        if ( confirm ){
          this.homeService.resetView();
        }
      })
      .catch(( err ) =>{
        console.error( err );
      });
  }

  sendViewParams() {
    this.isLoading = true;

    this.homeService.saveView()
      .then(() =>{
        const modalRef = this.modalService.open( ModalConfirmComponent );
        ( <ModalConfirmComponent>modalRef.componentInstance ).message = `La mise à jour de la vue a été effectuée avec succès`;
      })
      .catch(( err ) =>{
        console.error( err );
        this.loggerService.errorOnModal$( `Erreur survenue pendant la sauvegarde` );
      })
      .finally(() =>{
        this.isLoading = false;
      });
  }

}
