import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreviewModalComponent } from '../../core/components/preview-modal/preview-modal.component';
import { FormsModule } from '@angular/forms';
import { ManageComponent } from '../manage/manage.component';
import { HeaderComponent } from '../../core/components/header/header.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector:    `alk-custom`,
  standalone:  true,
  imports:     [ CommonModule, PreviewModalComponent, FormsModule, HeaderComponent, NgbTooltipModule ],
  templateUrl: `./custom.component.html`,
})
export class CustomComponent extends ManageComponent implements OnInit {


  constructor() {
    super();
  }

  override ngOnInit() {


    const uuid = this.activatedRoute.snapshot.paramMap.get( `uuid` );

    if ( uuid ){
      this.homeService.loadToView( uuid, `custom` );
    }
  }
}
