import { Component, inject, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from '../../core/components/base/base.component';
import { LayerResource } from '../../core/models/layer-resource';
import { ActivatedRoute, Router } from '@angular/router';
import { VuesService } from '../../core/services/prodige-api/vues.service';
import { of, switchMap, takeUntil } from 'rxjs';
import { JoinListComponent } from '../../core/components/join-list/join-list.component';
import { JoinModalComponent } from '../../core/components/join-modal/join-modal.component';
import { FormsModule } from '@angular/forms';
import { TViewType } from '../../core/models/view-type.model';
import { HeaderComponent } from '../../core/components/header/header.component';
import { UserService } from '../../core/services/user.service';

@Component({
  selector:    `alk-create`,
  standalone:  true,
  imports:     [ CommonModule, JoinListComponent, JoinModalComponent, FormsModule, HeaderComponent ],
  templateUrl: `./create.component.html`,
})
export class CreateComponent extends BaseComponent implements OnInit{
  // liste des layers
  protected layerRessources: LayerResource[] = [];

  // layer associer à la vue
  protected layerView: LayerResource = null;

  // série de donnée utilisé par la vue
  protected dataset: string = null;

  protected loadingOk = false;

  protected selectedViewType : TViewType = null;

  protected activatedRoute: ActivatedRoute;
  protected vueService: VuesService;
  protected router: Router;
  protected userService: UserService;

  constructor() {
    super();

    this.activatedRoute = inject( ActivatedRoute );
    this.vueService = inject( VuesService );
    this.router = inject( Router );
  }

  ngOnInit(): void {
    this.activatedRoute.data.pipe(
      takeUntil( this.endSubscriptions ),
      switchMap(( data ) =>  {
        if ( data[`layerRessources`] && data[`layerRessources`] instanceof Array && data[`layerView`]) {
          this.layerView =  <LayerResource>data[`layerView`];

          this.layerRessources = ( <LayerResource[]>data[`layerRessources`]).filter(( layerResource ) =>
            layerResource.id !== this.layerView.id,
          )
            .sort(( a, b ) => a.name.localeCompare( b.name ));

          return this.vueService.getViewParams$( true, this.layerView.metadataUuid );
        }
        return of( null );
      }),
    )
      .subscribe(( viewParams ) => {
        if ( viewParams ){
          this.router.navigate([ viewParams.is_customized ? `custom` : `manage`, this.layerView.metadataUuid ]).catch( console.error );
        }
        else {
          this.loadingOk = true;
        }
      });



  }

  validate(){
    if ( !this.layerView || !this.dataset ){
      return;
    }

    this.vueService.postView( this.layerView.storagePath, this.layerView.metadataUuid, this.dataset, this.selectedViewType === `custom` ).pipe(
      takeUntil( this.endSubscriptions ),
    )
      .subscribe({
        next: () => {
          this.vueService.clearViewParms();

          const route = this.selectedViewType === `custom` ? `custom` : `manage`;
          this.router.navigate([ route, this.layerView.metadataUuid ]).catch( console.error );
        },
      });
  }

}
