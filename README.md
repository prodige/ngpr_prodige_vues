# ngpr_prodige_vues

> :warning: **Ne pas jouer les commandes AngularCli (`ng ...`) ou `npm install` depuis sa machine.**
Se connecter au container docker pour le faire. Celui-ci possède les bonnes versions de node, npm, angular-cli, typescript ( les mêmes que les autres personnes travaillant sur le projet).

## Pour démarrer de Container docker de dev
container démarré par pr_prodige v5

## Pour se connecter au Container docker de dev
Pour ajouter des dépendances via `npm install [nom de la dépendance]` ou pour générer des éléments avec des commandes `ng ...` par exemple.
```bash
docker exec --user node -w /home/node/app/site -it ngpr_prodige_vues_web bash
```

## Pour l'installation (depuis le container)
```bash
npm run login
npm install
```

## Pour lancer le serveur
```bash
docker exec --user node -w /home/node/app/site -it ngpr_prodige_vues_web npm run dev
```

## A propos d'Angular 16
ce projet utilise angular 16 avec le nouveau system de component standalone
https://angular.io/guide/standalone-components

les templates peuvent maintenant utilisé les variables protected du component
