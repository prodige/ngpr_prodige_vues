#!/bin/bash
set -e

SCRIPTDIR="$( cd $(dirname ${BASH_SOURCE[0]}/) && pwd )"
PROJECTDIR="$( cd ${SCRIPTDIR}/../../..  && pwd )"
PROJECTNAME="$(basename $PROJECTDIR )"
NODE_CONTAINER_NAME="${PROJECTNAME}_dev_node"

# Si le docker tourne, il faut d'abord l'éteindre pour pouvoir supprimer le volume
if [ ! "$( docker container inspect -f '{{.State.Status}}' $NODE_CONTAINER_NAME )" == "running" ]; then
    echo -e "docker container $NODE_CONTAINER_NAME is not running"
    exit
fi

# Ouvre l'interpreteur de commande bash directement à la racine du projet angular après initialisation
docker exec -it --user node -w /home/node/app ${NODE_CONTAINER_NAME} bash;
