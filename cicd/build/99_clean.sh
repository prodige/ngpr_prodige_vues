#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

#if [ ! "$(docker ps -q -f name=$DOCKER_NAME)" ]; then
#    if [ "$(docker ps -aq -f status=exited -f name=$DOCKER_NAME)" ]; then
#        # Clean docker
#        docker rm -f $DOCKER_NAME
#    fi
#    # Create docker
#    docker run \
#      -d \
#      --name $DOCKER_NAME \
#      -v "$SCRIPTDIR/../..":/home/node/app \
#      -w /home/node/app \
#      node:12-stretch \
#      sh -c 'while sleep 3600; do :; done'
#fi
echo "  Start 99_clean.sh"

echo -n "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > $SCRIPTDIR/docker-build/docker-compose.env;

# Clean generated containers
list="${DOCKER_NAME}_web ${DOCKER_NAME}_build_web ${DOCKER_NAME}_trivy"
for var in $list
do
  if [ "$(docker ps -aq -f name=${var})" ]; then
    echo "  Clean container ${var}"
    docker rm -f ${var}
  fi

done

# Clean generated images
list="${DOCKER_NAME}_build_web"
for var in $list
do
  if [ "$(docker images -q -f reference=${var})" ]; then
    echo "  Clean images ${var}"
    docker rmi ${var}
  fi
done

echo "  End 99_clean.sh"
