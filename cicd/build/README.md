# Build

## Execute build process locally

The build step is a pipeline described in ```Jenkinsfile```.

The steps are run in order with all bash scripts as [number]_[script_name].sh

Example, run one by one :
```bash
sh ./0_fetch-dependencies.sh
sh ./1_lint.sh
sh ./3_build-prod.sh
sh ./5_make_release.sh
sh ./6_make_docker.sh
```

Now you can run the docker you built with ```../test-prod/README.md```

If an error occurs, use the ```99_clean.sh``` to clean all and revert rights.

## Advanced

### Use nexus

To use nexus.alkante.com get composer and/or npm package, you need to uncomment lines with NEXUS, NEXUS_USR and NEXUS_PSW in file:
- ./0_fetch-dependencies.sh
- ./Jenkinsfile

### Run assetic

To run assetic, you need to uncomment "Make asset cache" step in :
- ./Jenkinsfile
