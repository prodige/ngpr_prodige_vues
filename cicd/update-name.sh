#!/bin/bash
# Script pour mettre à jour le nom du projet
# en se basant sur le nom de dossier se trouvant
# avant le dossier cicd
# Exemple pour /www/spr_my_new_ngpr_prodige_vues/cicd/
# oldName = tpl_alkante_symfony_4
# newName = spr_my_new_ngpr_prodige_vues
#
# Usage : ./update-name.sh

cicdPath=$(dirname $(readlink -f $0))
oldName=$(cat $cicdPath/docker_name.txt)
newName=$(basename $(dirname $cicdPath))

echo $cicdPath
echo $oldName
echo $newName
if [[ "$oldName" != "" && "$newName" != "" ]]; then
	echo "$oldName -> $newName"
	grep -rl "$oldName" .. | xargs sed -i "s/$oldName/$newName/g"
	echo -n "$newName" > $cicdPath/docker_name.txt
else
	echo "Erreur de nom"
	echo "oldName: '$oldName'"
	echo "newName: '$newName'"
fi
