(function (window) {
  window.__env = window.__env || {};

  // Whether to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = true;

  // Api Info
  window.__env.apiUrl = "http://localhost:3000";
}(this));
