#!/bin/bash
set -e

SCRIPTDIR="$( cd $(dirname ${BASH_SOURCE[0]}/) && pwd )"
PROJECTDIR="$( cd ${SCRIPTDIR}/../..  && pwd )"
PROJECTNAME="$(basename $PROJECTDIR )"
NGINX_CONTAINER_NAME="${PROJECTNAME}_serve_prod_web"

if [ "$1" == "debug" ]; then
  echo -e "SCRIPTDIR=${SCRIPTDIR}";
  echo -e "PROJECTDIR=${PROJECTDIR}";
  echo -e "PROJECTNAME=${PROJECTNAME}";
  echo -e "NGINX_CONTAINER_NAME=${NGINX_CONTAINER_NAME}";
  exit
fi

# On se place dans le répertoire du script (cicd/dev)
cd ${SCRIPTDIR}

# ID user for mapping in docker
echo -e "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > docker-compose.env

if [ "$1" == "down" ]; then
  # Stop and remove containers, networks, images, and volumes
  echo -e "docker-compose down -t 1";
  docker-compose down -t 1;
  exit;
elif [ "$1" == "build" ]; then
  if [ "$( docker container inspect -f '{{.State.Status}}' ${NGINX_CONTAINER_NAME} )" == "running" ]; then
    # Stop and remove containers, networks, images, and volumes
    echo -e "docker-compose down -t 1";
    docker-compose down -t 1;
  fi
  echo -e "docker-compose build";
  docker-compose build
  exit;
else
  # On down le précédent container si déjà lancé
  docker-compose down -t 1;

  # Run container
  echo -e "docker-compose up";
  docker-compose up
fi
