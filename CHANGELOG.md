# Changelog

All notable changes to [ngpr_prodige_vues](https://gitlab.adullact.net/prodige/ngpr_prodige_vues) project will be documented in this file.

## [5.0.14](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.13...5.0.14) - 2024-12-12

## [5.0.13](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.12...5.0.13) - 2024-12-06

## [5.0.12](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.11...5.0.12) - 2024-12-05

## [5.0.11](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.10...5.0.11) - 2024-11-29

## [5.0.10](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.9...5.0.10) - 2024-10-22

## [5.0.9](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.8...5.0.9) - 2024-10-21

## [5.0.8](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.7...5.0.8) - 2024-10-18

## [5.0.7](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.6...5.0.7) - 2024-03-08

## [5.0.6](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.5...5.0.6) - 2024-02-27

## [5.0.5](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.4...5.0.5) - 2024-02-23

## [5.0.4](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.3...5.0.4) - 2024-02-19

## [5.0.3](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.2...5.0.3) - 2023-12-19

## [5.0.3](https://gitlab.adullact.net/prodige/ngpr_prodige_vues/compare/5.0.1...5.0.3) - 2023-12-19

